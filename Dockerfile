#FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build


WORKDIR /src
COPY . .

COPY ["Avione.Search.gRpcServices/Avione.Search.gRpcServices.csproj", "Avione.Search.gRpcServices/"]
COPY ["Avione.Search.ETM/Avione.Infrastructure.Api.Etm.csproj", "Avione.Search.ETM/"]
COPY ["Avione.Search.Presentation/Avione.Search.Presentation.csproj", "Avione.Search.Presentation/"]
COPY ["Avione.Search.gRpcProtos/Avione.Search.gRpcProtos.csproj", "Avione.Search.gRpcProtos/"]
# COPY ["Avione.API/Avione.Search.Business/Avione.Search.Business.csproj", "Avione.Search.Business/"]
COPY ["Avione.Search.Core/Avione.Search.Core.csproj", "Avione.Search.Core/"]

COPY ["Avione.Search.gRpcProtos/Protos/", "Avione.Search.gRpcProtos/Protos/"]

# COPY ["Avione.API/Avione.Search.gRpcProtos/Protos/common.proto", "Avione.Search.gRpcProtos/Protos/"]
# COPY ["Avione.API/Avione.Search.gRpcProtos/Protos/iata.proto", "Avione.Search.gRpcProtos/Protos/"]
# COPY ["Avione.API/Avione.Search.gRpcProtos/Protos/login.proto", "Avione.Search.gRpcProtos/Protos/"]
# COPY ["Avione.API/Avione.Search.gRpcProtos/Protos/search.proto", "Avione.Search.gRpcProtos/Protos/"]
# COPY ["Avione.API/Avione.Search.gRpcProtos/Protos/offer.proto", "Avione.Search.gRpcProtos/Protos/"]
# COPY ["Avione.API/Avione.Search.gRpcProtos/Protos/user.proto", "Avione.Search.gRpcProtos/Protos/"]
# COPY ["Avione.API/Avione.Search.gRpcProtos/Protos/collection.proto", "Avione.Search.gRpcProtos/Protos/"]
# COPY ["Avione.API/Avione.Search.gRpcProtos/Protos/search_models.proto", "Avione.Search.gRpcProtos/Protos/"]

RUN dotnet restore "Avione.Search.gRpcServices/Avione.Search.gRpcServices.csproj"

WORKDIR "/src/Avione.Search.gRpcServices"
RUN dotnet build "Avione.Search.gRpcServices.csproj" -c Release -o /app/build && dotnet publish "Avione.Search.gRpcServices.csproj" -c Release -o /app/publish


#FROM mcr.microsoft.com/dotnet/aspnet:5.0
#FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim
FROM mcr.microsoft.com/dotnet/aspnet:5.0


WORKDIR /app
COPY --from=build /app/publish .
ENTRYPOINT ["dotnet", "Avione.Search.gRpcServices.dll"]
