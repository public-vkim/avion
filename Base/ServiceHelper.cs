﻿using System;
using Grpc.Core;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Nest;

namespace Avione.Search.gRpcServices.Base
{
    public static class ServiceHelper
    {
        public static int CurrentUserId(this ServerCallContext context)
        {
            var nameIdentifier = context.GetHttpContext().User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (string.IsNullOrWhiteSpace(nameIdentifier))
                throw new ArgumentException("Пользователь не найден.");


            var success = int.TryParse(nameIdentifier, out int userId);
            if (!success || (userId <= 0))
                throw new ArgumentException("Пользователь не найден.");

            return userId;
        }

        public static int TryGetCurrentUserId(this ServerCallContext context)
        {
            try
            {
                var nameIdentifier = context.GetHttpContext().User.FindFirstValue(ClaimTypes.NameIdentifier);
                if (string.IsNullOrWhiteSpace(nameIdentifier))
                    return 0;


                var success = int.TryParse(nameIdentifier, out int userId);
                if (!success || (userId <= 0))
                    return 0;

                return userId;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static int? TryToGetAppuserUserId(this ServerCallContext context)
        {
            try
            {
                var nameIdentifier = context.GetHttpContext().User.FindFirstValue(ClaimTypes.NameIdentifier);
                if (string.IsNullOrWhiteSpace(nameIdentifier))
                    return null;


                var success = int.TryParse(nameIdentifier, out int userId);
                if (!success || (userId <= 0))
                    return null;

                return userId;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string CurrentDevice(this ServerCallContext context)
        {
            try
            {
                var deviceName = context.RequestHeaders?.FirstOrDefault(p => p.Key.Equals("DeviceName", StringComparison.CurrentCultureIgnoreCase));
                if (deviceName == null)
                    return "Mobile App";

                return deviceName?.Value;
            }
            catch (Exception)
            {

                return "Mobile App";
            }

        }
    }
}
