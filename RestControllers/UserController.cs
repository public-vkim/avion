﻿using Avione.Common.gRpcProtos;
using Avione.User.gRpcProtos;
using Grpc.AspNetCore.Server;
using Grpc.Core.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

/// <summary>
/// Summary description for Class1
/// </summary>
/// 


[ApiController]
[Route("api/[controller]")]

public class UserController : ControllerBase
{
    
    private readonly gRpcUserService.gRpcUserServiceClient _grpcClient; 
    private readonly IConfiguration _configuration; 
    private readonly ILogger<UserController> _logger;   



    public UserController(gRpcUserService.gRpcUserServiceClient grpcClient, 
        IConfiguration configuration, ILogger<UserController> logger)
    {
        _grpcClient = grpcClient; 
        _configuration  = configuration; 
        _logger = logger;
        
     
    }

    [HttpPost("send-code")] 

    public async Task <IActionResult> SendMeCode([FromBody] SendMeCodeRequest request)
    {
       
        var grpcRequest = new SendMeCodeRequest
        {
            PhoneNumber = request.PhoneNumber,
            PhoneNumberCode = request.PhoneNumberCode,
            UserVerificationType = request.UserVerificationType,
            

        };

        if(request.UserVerificationType == UserVerificationTypeClient.Default)
        {
            return BadRequest("Неизвестный тип подтверждения пользователя");

        }
        
        
        if (string.IsNullOrEmpty(request.PhoneNumber) || string.IsNullOrEmpty(request.PhoneNumberCode))
            throw new ArgumentException("The phone number and country code are not specified");


        var grpcResponse = await _grpcClient.SendMeCodeAsync(grpcRequest);
        return Ok(grpcResponse);
        
    }

    [HttpPost("Verify-phone")]

    public async Task<IActionResult> VerifyMe([FromBody] VerifyPhoneNumberRequest numberRequest)
    {
        var numRequest = new VerifyPhoneNumberRequest
        {
            PhoneNumber = numberRequest.PhoneNumber,
            PhoneNumberCode = numberRequest.PhoneNumberCode,
            SMSCode = numberRequest.SMSCode,
        };

        if (string.IsNullOrEmpty(numberRequest.PhoneNumber) || string.IsNullOrEmpty(numberRequest.PhoneNumberCode))
            throw new ArgumentException("The phone number and country code are not specified");

        if (string.IsNullOrEmpty(numberRequest.SMSCode))
            throw new ArgumentException("SMSCode is required");

        var grpcResponse = await _grpcClient.VerifyMeAsync(numberRequest);
        return Ok(grpcResponse);
    }


    [HttpPost("Login")] 

    public async Task<IActionResult> LogingRequest([FromBody] LoginRequest logrequest)
    {
        var logreq = new LoginRequest
        {
            PhoneNumber = logrequest.PhoneNumber,
            PhoneNumberCode = logrequest.PhoneNumberCode,
            Password = logrequest.Password,
        };

        if (string.IsNullOrEmpty(logrequest.PhoneNumber) || string.IsNullOrEmpty(logrequest.PhoneNumberCode))
            throw new ArgumentException("The phone number and country code are not specified");

        if (string.IsNullOrEmpty(logrequest.Password))
            throw new ArgumentException("Password is required");
        var grpcResponse = await _grpcClient.LoginAsync(logreq);
        return Ok(grpcResponse);
    }

    [HttpPost("Registration")]

    public async Task<IActionResult> RegisterRequest([FromBody] RegisterRequest registerrequest)
    {
        var registrationreq = new RegisterRequest
        {
            PhoneNumber = registerrequest.PhoneNumber,
            PhoneNumberInfo= registerrequest.PhoneNumberInfo, 
            SMSCode = registerrequest.SMSCode,
            Password = registerrequest.Password,
            PasswordConfirm =   registerrequest.PasswordConfirm, 
            FirstName = registerrequest.FirstName, 
            LastName = registerrequest.LastName, 
            MiddleName = registerrequest.MiddleName,
            DateOfBirth = registerrequest.DateOfBirth,
            Gender = registerrequest.Gender, 
            Email = registerrequest.Email,
            ExtraPhoneNumber = registerrequest.ExtraPhoneNumber,
            Country = registerrequest.Country,
            Address = registerrequest.Address,
            City = registerrequest.City,
            Zip = registerrequest.Zip,
            Document = registerrequest.Document, 
            
        
        };
       
        if (string.IsNullOrEmpty(registerrequest.PhoneNumber))
            throw new ArgumentException("The phone number is required");
      
        if (registerrequest.PhoneNumberInfo == null || string.IsNullOrEmpty (registerrequest.PhoneNumberInfo.Code))
        throw new ArgumentException("The phone number code is not specified");

        if (string.IsNullOrEmpty(registerrequest.FirstName))
            throw new ArgumentException("The name is required"); 

        if(string.IsNullOrEmpty(registerrequest.LastName)) 
            throw new ArgumentException("The last name is required");

        if (string.IsNullOrEmpty(registerrequest.MiddleName))
            throw new ArgumentException("The MiddleName is required");

        if ((registerrequest.Gender == null)) 
            throw new ArgumentException("Gender is required");

        if (string.IsNullOrEmpty(registrationreq.DateOfBirth))
            throw new ArgumentException("Date of birth is required");  


        
        var grpcResponse = await _grpcClient.RegisterAsync(registerrequest); 
        return Ok(grpcResponse);
    }

}
