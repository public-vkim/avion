﻿using Avione.BizLogicLayer.Flight;
using Avione.Infrastructure.Api.Etm.Client;
using Avione.BizLogicLayer.Flight.Enums;
using Avione.BizLogicLayer.Flight.Settings;
using Avione.Infrastructure.DbContext.Context;
using Avione.Infrastructure.DbContext.Entities;
using Avione.Search.gRpcServices.Base;
using Avione.Search.gRpcServices.Services;
using Avione.ServiceLayer;
using Avione.ServiceLayer.Settings;
using Avione.SearchApi.Services;
using ETM.Reference;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;
using Avione.User.gRpcProtos;

namespace Avione.Search.gRpcServices
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddGrpc(options => {
                options.EnableDetailedErrors = true;
            }); 

            services.AddGrpcClient<gRpcUserService.gRpcUserServiceClient>(options =>
            {
                options.Address = new Uri("https://localhost:5001");
            }
                );
            //services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "MobileApi",
                    Version = "v1",
                });
            });
            
             
            //Settings. could not move it into StartupHelper
            services.Configure<ETMSettings>(Configuration.GetSection(nameof(ETMSettings)));
            services.Configure<DAETMSettings>(Configuration.GetSection(nameof(DAETMSettings)));
            services.Configure<EBETMSettings>(Configuration.GetSection(nameof(EBETMSettings)));
            services.Configure<TokenSettings>(Configuration.GetSection(nameof(TokenSettings)));
            services.Configure<EmailSettings>(Configuration.GetSection(nameof(EmailSettings)));
            services.Configure<SMSSettings>(Configuration.GetSection(nameof(SMSSettings)));
            services.Configure<SupportSettings>(Configuration.GetSection(nameof(SupportSettings)));
            services.Configure<PaymoSettings>(Configuration.GetSection(nameof(PaymoSettings)));
            services.Configure<CommonSettings>(Configuration.GetSection(nameof(CommonSettings)));
            services.Configure<DataSourceSettings>(Configuration.GetSection(nameof(DataSourceSettings)));
            services.Configure<ApelsinSettings>(Configuration.GetSection(nameof(ApelsinSettings)));
            services.Configure<VipServiceSettings>(Configuration.GetSection(nameof(VipServiceSettings)));

            StartupHelper.ConfigureAvioneServices(services, Configuration);
            StartupHelper.ConfigureJwtAuthentication(services, Configuration);

            services.AddHttpContextAccessor();

            // gRpc RailwayOrderDataService
            services.AddScoped<RailwayOrderdataService>();
            //services.AddScoped<gRpcUserService.gRpcUserServiceClient>();
        }  

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AppDataContext appDataContext, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage(); 

                 app.UseSwagger();
            app.UseSwaggerUI(c => 
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MobileApi v1");
                c.RoutePrefix = string.Empty;
            });
            }

            app.UseRouting();

            //Login and get session key for each request.
            app.UseMiddleware<LoginMiddleware>();

            appDataContext.Database.Migrate();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<IATAService>();
                endpoints.MapGrpcService<AvioneUserService>();
                endpoints.MapGrpcService<AviaSearchService>();
                endpoints.MapGrpcService<SearchApi.Services.OfferService>();
                endpoints.MapGrpcService<AviaOrderService>();
                endpoints.MapGrpcService<CollectionService>();
                endpoints.MapGrpcService<RailwaySearchService>();
                endpoints.MapGrpcService<RailwayOrderService>();
                endpoints.MapGrpcService<RailwayOrderdataService>();
                endpoints.MapGrpcService<ContentService>();
                endpoints.MapControllerRoute(name: "user", 
                    pattern: "{controller = User}/{action=index}/{id}"
                    );
                

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });

           // CreateUserRoles(serviceProvider).Wait();
        }

        private async Task CreateUserRoles(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<SystemUser>>();

            IdentityResult roleResult;

            //Adding Admin Role
            var roleCheck = await RoleManager.RoleExistsAsync(UserRoles.Admin.ToString());
            if (!roleCheck)
            {
                //create the roles and seed them to the database
                roleResult = await RoleManager.CreateAsync(new IdentityRole(UserRoles.Admin.ToString()));
            }

            //roleCheck = await RoleManager.RoleExistsAsync(UserRoles.Moderator.ToString());
            //if (!roleCheck)
            //{
            //    //create the roles and seed them to the database
            //    roleResult = await RoleManager.CreateAsync(new IdentityRole(UserRoles.Moderator.ToString()));
            //}

            SuperUserSettings superUserSettings = Configuration.GetSection(nameof(SuperUserSettings)).Get<SuperUserSettings>();

            //Assign Admin role to the main User here we have given our newly registered 
            //login id for Admin management
            SystemUser user = await UserManager.FindByEmailAsync(superUserSettings.Email);
            if (user == null)
            {
                user = new SystemUser()
                {
                    FirstName = superUserSettings.FirstName,
                    LastName = superUserSettings.LastName,
                    UserName = superUserSettings.Email,
                    Email = superUserSettings.Email
                };
                await UserManager.CreateAsync(user, superUserSettings.Password);

                user = await UserManager.FindByEmailAsync(superUserSettings.Email);
                await UserManager.AddToRoleAsync(user, UserRoles.Admin.ToString());
            }
        }
    }
}
