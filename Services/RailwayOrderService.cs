﻿using AutoMapper;
using Avione.Common.gRpcProtos;
using Avione.RailwayOrder.gRpcProtos;
using Avione.Search.gRpcServices.Base;
using Avione.ServiceLayer.Constants;
using Avione.ServiceLayer.Models.Inputs.Railway;
using Avione.ServiceLayer.Services.Order;
using Avione.ServiceLayer.Services.Order.Railway;
using Grpc.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avione.Search.gRpcServices.Services
{
    public class RailwayOrderService : gRpcRailwayOrder.gRpcRailwayOrderBase
    {
        private readonly IUTYOrderService utyOrderService;
        private readonly IRailwayOrderPaymentService railwayOrderPaymentService;
        private readonly IMapper mapper;
        private readonly RailwayOrderdataService railwayOrderdataService;
        private readonly IUTYOrderDataService utyOrderdataService;
        private readonly ILogger<RailwayOrderService> logger;
        private readonly IHttpContextAccessor _accessor;

        public RailwayOrderService(
            IUTYOrderService utyOrderService,
            IRailwayOrderPaymentService railwayOrderPaymentService,
            IMapper mapper,
            RailwayOrderdataService railwayOrderdataService,
            IUTYOrderDataService utyOrderdataService,
            IHttpContextAccessor accessor,
            ILogger<RailwayOrderService> logger
            )
        {
            this.utyOrderService = utyOrderService;
            this.railwayOrderPaymentService = railwayOrderPaymentService;
            this.mapper = mapper;
            this.railwayOrderdataService = railwayOrderdataService;
            this.utyOrderdataService = utyOrderdataService;
            this.logger = logger;
            this._accessor = accessor;
            this._accessor.HttpContext.Request.Headers["DeviceType"] = CommonConstants.Mobile;
        }

        public override Task<DoOrderResponseDto> DoOrder(DoOrderInputRequest request, ServerCallContext context)
        {
            try
            {
                int? appUserId = context.TryToGetAppuserUserId();

                if (appUserId is not null)
                    utyOrderService.CurrentAppUserId = appUserId;


                var orderRequest = mapper.Map<DoOrderInput>(request);

                var result = utyOrderService.DoOrder(orderRequest).GetAwaiter().GetResult();
                DoOrderResponseDto doOrderResponseDto = new DoOrderResponseDto();
                if (result.Error != null)
                {
                    doOrderResponseDto.Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Error = result.Error?.error ?? "Error in Doing order",
                        Message = result.Error?.message ?? "Error in Doing order",
                        Path = result.Error?.path ?? string.Empty,
                        Status = result.Error.status
                    };
                }
                else
                {
                    List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO> railwayOrderDTOs = new List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO>();
                    railwayOrderDTOs.Add(result.Order);

                    doOrderResponseDto.Order = railwayOrderdataService.CustomizeOrder(railwayOrderDTOs).FirstOrDefault();

                }

                return Task.FromResult(doOrderResponseDto);
            }
            catch (System.Exception ex)
            {
                return Task.FromResult(new DoOrderResponseDto()
                {
                    Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Message = ex.Message,
                        Error = ex.InnerException?.Message
                    }
                });
            }
        }
        public override async Task<CancelOrderResponse> CancelOrder(CancelOrderRequest request, ServerCallContext context)
        {
            try
            {
                var result = await utyOrderService.CancelOrder(request.OrderId);
                CancelOrderResponse cancelOrder = new CancelOrderResponse();
                if (result.Error != null)
                {
                    cancelOrder.ErrorModel = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Error = result?.Error?.error ?? "Error in Cancelling Order",
                        Message = result?.Error?.message ?? $"Error in Cancelling Railway Order,OrderId : {request.OrderId}",
                        Path = result?.Error?.path ?? "railway/cancel-order",
                        Status = result?.Error?.status ?? 500

                    };
                    return cancelOrder;
                }

                cancelOrder.OrderId = request.OrderId;
                cancelOrder.Status = ResponseStatusType.Ok;
                return cancelOrder;
            }
            catch (Exception ex)
            {
                return new CancelOrderResponse
                {
                    OrderId = request.OrderId,
                    Status = ResponseStatusType.Error,
                    ErrorModel = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Error = ex.Message,
                        Message = ex.Message,
                        Path = ex.InnerException?.Message,
                        Status = ex.HResult
                    }
                };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<OrderResponseDto> GetOrderById(OrderIdRequest request, ServerCallContext context)
        {
            try
            {
                utyOrderService.CurrentUserId = context.CurrentUserId();

                var getOrderByIdResult = utyOrderService.GetOrderById(request.OrderId).GetAwaiter().GetResult();

                OrderResponseDto orderResponseDto = new OrderResponseDto();
                if (getOrderByIdResult.Error != null)
                {
                    orderResponseDto.Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Error = getOrderByIdResult.Error.error,
                        Message = getOrderByIdResult.Error.message,
                        Path = getOrderByIdResult.Error.path,
                        Status = getOrderByIdResult.Error.status
                    };

                    return Task.FromResult(orderResponseDto);
                }

                List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO> railwayOrderDTOs = new List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO>();
                railwayOrderDTOs.Add(getOrderByIdResult.Order);

                var customizedOrder = railwayOrderdataService.CustomizeOrder(railwayOrderDTOs);
                if (customizedOrder != null && customizedOrder.Count != 0)
                {
                    orderResponseDto.Order = customizedOrder[0];
                }

                return Task.FromResult(orderResponseDto);

            }
            catch (System.Exception ex)
            {

                return Task.FromResult(new OrderResponseDto()
                {
                    Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Message = ex.Message,
                        Error = ex.InnerException?.Message
                    }
                });

            }

        }

        public override async Task<DeleteOrderResponse> DeleteOrder(OrderIdRequest request, ServerCallContext context)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.OrderId))
                {
                    return new DeleteOrderResponse
                    {
                        Status = ResponseStatusType.Error,
                        Message = "OrderId is null , It is must",
                    };
                }

                var deleteOrderResponse = await utyOrderdataService.DeleteOrderAsync(request.OrderId);

                if (deleteOrderResponse.Status == BizLogicLayer.Flight.Enums.StatusEnum.Ok)
                {
                    return new DeleteOrderResponse
                    {
                        Status = ResponseStatusType.Ok
                    };
                }
                else
                {
                    return new DeleteOrderResponse
                    {
                        Status = ResponseStatusType.Error,
                        Message = deleteOrderResponse.Message,
                    };
                }

            }
            catch (Exception ex)
            {
                return new DeleteOrderResponse
                {
                    Status = ResponseStatusType.Error,
                    Message = ex.Message,
                };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

        public override Task<AppUserOrdersResponse> GetAppUserOrders(EmptyRequest request, ServerCallContext context)
        {
            try
            {
                utyOrderService.CurrentUserId = context.CurrentUserId();

                var appUserOrders = utyOrderService.GetAppUserOrders();

                List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO> railwayOrderDTOs = new List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO>();
                railwayOrderDTOs.AddRange(appUserOrders);

                var customizedOrders = railwayOrderdataService.CustomizeOrder(railwayOrderDTOs);

                AppUserOrdersResponse appUserOrdersResponse = new AppUserOrdersResponse();
                appUserOrdersResponse.Orders.AddRange(customizedOrders);

                return Task.FromResult(appUserOrdersResponse);

            }
            catch (System.Exception ex)
            {
                throw new System.ArgumentException(ex.Message);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<OrderResponseDto> GetOrderByIdFromDb(OrderIdRequest request, ServerCallContext context)
        {
            try
            {
                utyOrderService.CurrentUserId = context.CurrentUserId();

                var getOrderByIdResult = utyOrderService.GetOrderByIdFromDb(request.OrderId).GetAwaiter().GetResult();

                OrderResponseDto orderResponseDto = new OrderResponseDto();
                if (getOrderByIdResult.Error != null)
                {
                    orderResponseDto.Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Error = getOrderByIdResult.Error.error,
                        Message = getOrderByIdResult.Error.message,
                        Path = getOrderByIdResult.Error.path,
                        Status = getOrderByIdResult.Error.status
                    };

                    return Task.FromResult(orderResponseDto);
                }

                List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO> railwayOrderDTOs = new List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO>();
                railwayOrderDTOs.Add(getOrderByIdResult.Order);

                var customizedOrder = railwayOrderdataService.CustomizeOrder(railwayOrderDTOs);
                if (customizedOrder != null && customizedOrder.Count != 0)
                {
                    orderResponseDto.Order = customizedOrder[0];
                }

                return Task.FromResult(orderResponseDto);

            }
            catch (System.Exception ex)
            {

                return Task.FromResult(new OrderResponseDto()
                {
                    Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Message = ex.Message,
                        Error = ex.InnerException?.Message
                    }
                });

            }
        }

        public override Task<EndTimeResponseDto> EndTime(OrderIdRequest request, ServerCallContext context)
        {
            try
            {
                utyOrderService.CurrentUserId = context.CurrentUserId();

                EndTimeResponseDto endTimeResponse = new EndTimeResponseDto();

                var endTimeResult = utyOrderService.EndTime(request.OrderId).GetAwaiter().GetResult();
                if (endTimeResult.Error != null)
                {
                    endTimeResponse.Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Error = endTimeResult.Error?.error ?? string.Empty,
                        Message = endTimeResult.Error?.message ?? string.Empty,
                        Path = endTimeResult.Error?.path ?? string.Empty,
                        Status = endTimeResult.Error.status

                    };
                }
                else
                {
                    endTimeResponse.EndTime = new EndTimeDto
                    {
                        OrderId = endTimeResult?.EndTime?.OrderId ?? string.Empty,
                        Status = endTimeResult?.EndTime?.Status ?? "Error",
                        Seconds = endTimeResult?.EndTime?.Seconds ?? 360,
                    };
                }


                return Task.FromResult(endTimeResponse);
            }
            catch (System.Exception ex)
            {

                return Task.FromResult(new EndTimeResponseDto()
                {
                    Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Message = ex.Message,
                        Error = ex.InnerException?.Message
                    }
                });
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<ChoosePaymentResponseDto> ChoosePayment(OrderIdRequest request, ServerCallContext context)
        {
            try
            {
                utyOrderService.CurrentUserId = context.CurrentUserId();

                var choosePaymentResult = utyOrderService.ChoosePayment(new ChoosePaymentInput { OrderId = request.OrderId }).GetAwaiter().GetResult();

                var choosePaymentResponse = mapper.Map<ChoosePaymentResponseDto>(choosePaymentResult);

                return Task.FromResult(choosePaymentResponse);
            }
            catch (System.Exception ex)
            {

                return Task.FromResult(new ChoosePaymentResponseDto()
                {
                    Error = new RailwaySearch.gRpcProtos.ErrorModelDto
                    {
                        Message = ex.Message,
                        Error = ex.InnerException?.Message
                    }
                });
            }


        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<PaymentDataResponseDto> GetPaymentData(OrderIdRequest request, ServerCallContext context)
        {
            try
            {
                railwayOrderPaymentService.CurrentUser = context.CurrentUserId();

                var getPaymentDataResult = railwayOrderPaymentService.GetPaymentData(request.OrderId).GetAwaiter().GetResult();

                PaymentDataResponseDto paymentDataResponseDto = new PaymentDataResponseDto();

                if (getPaymentDataResult.Status == BizLogicLayer.Flight.Enums.StatusEnum.Error)
                {
                    paymentDataResponseDto.Status = ResponseStatusType.Error;
                }
                else
                {
                    paymentDataResponseDto.Payment = new PaymentBody
                    {
                        Currency = getPaymentDataResult?.Data?.Currency ?? string.Empty,
                        FopType = getPaymentDataResult?.Data?.FopType ?? string.Empty,
                        SucessRedirectLink = getPaymentDataResult?.Data?.SucessRedirectLink ?? string.Empty,
                        TotalAmount = getPaymentDataResult?.Data?.TotalAmount ?? string.Empty
                    };
                }


                return Task.FromResult(paymentDataResponseDto);

            }
            catch (System.Exception ex)
            {
                return Task.FromResult(new PaymentDataResponseDto()
                {
                    Message = ex.Message,
                    Status = Common.gRpcProtos.ResponseStatusType.Error,
                });
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<PaymentDataResponseDto> GetPaymentInfo(RailwayPaymentRequest request, ServerCallContext context)
        {
            try
            {
                railwayOrderPaymentService.CurrentUser = context.CurrentUserId();

                RailwayPaymentInput railwayPayment = new RailwayPaymentInput
                {
                    OrderId = request.OrderId,
                    AppUserId = context.CurrentUserId(),
                    PaymentMethod = request.PaymentMethod,
                    AmountFromUserBalance = decimal.Parse(request.AmountFromUserBalance),
                    RequiredOnlinePayment = decimal.Parse(request.RequiredOnlinePayment)
                };

                var getPaymentDataResult = railwayOrderPaymentService.GetPaymentInfo(railwayPayment).GetAwaiter().GetResult();

                PaymentDataResponseDto paymentDataResponseDto = new PaymentDataResponseDto();

                if (getPaymentDataResult.Status == BizLogicLayer.Flight.Enums.StatusEnum.Error)
                {
                    paymentDataResponseDto.Status = ResponseStatusType.Error;
                }
                else
                {
                    paymentDataResponseDto.Payment = new PaymentBody
                    {
                        Currency = getPaymentDataResult?.Data?.Currency ?? string.Empty,
                        FopType = getPaymentDataResult?.Data?.FopType ?? string.Empty,
                        SucessRedirectLink = getPaymentDataResult?.Data?.SucessRedirectLink ?? string.Empty,
                        TotalAmount = getPaymentDataResult?.Data?.TotalAmount ?? string.Empty
                    };
                }

                return Task.FromResult(paymentDataResponseDto);
            }
            catch (System.Exception ex)
            {
                return Task.FromResult(new PaymentDataResponseDto()
                {
                    Message = ex.Message,
                    Status = Common.gRpcProtos.ResponseStatusType.Error,
                });
            }
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<ConnectOrdersResponse> ConnectOrdersToUser(RailwayOrder.gRpcProtos.OrdersArrayRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                logger.LogInformation($"::::: ConnectOrdersToUser Railway, UserId: {currentUserId} :::::");

                foreach (string orderId in request.OrdersIds)
                {
                    await utyOrderService.ConnectOrderToUser(orderId, currentUserId);
                }

                return new ConnectOrdersResponse { Status = ResponseStatusType.Ok };
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return new ConnectOrdersResponse { Status = ResponseStatusType.Error };
            }
        }
    }
}
