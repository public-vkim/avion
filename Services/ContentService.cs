﻿using AutoMapper;
using Avione.Common.gRpcProtos;
using Avione.Content.gRpcProtos;
using Avione.Infrastructure.DbContext.Entities;
using Avione.Infrastructure.DbContext.Repository;
using Avione.ServiceLayer.Constants;
using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Avione.Search.gRpcServices.Services
{
    public class ContentService : ContentRpcService.ContentRpcServiceBase
    {

        private readonly IRepository<PostEntity> postRepository;
        private readonly IRepository<CategoryEntity> categoryRepository;
        private readonly IMapper mapper;
        private readonly IHttpContextAccessor _accessor;

        public ContentService(IRepository<PostEntity> postRepository, IRepository<CategoryEntity> categoryRepository, IMapper mapper, IHttpContextAccessor accessor)
        {
            this.postRepository = postRepository;
            this.categoryRepository = categoryRepository;
            this.mapper = mapper;
            _accessor = accessor;
            _accessor.HttpContext.Request.Headers["DeviceType"] = CommonConstants.Mobile;
        }

        public override async Task<CategoriesModel> GetAllCategories(EmptyRequest request, ServerCallContext context)
        {
            var categories = await categoryRepository.GetAll().ToListAsync();

            var mappedCategories = mapper.Map<List<CategoryEntity>, List<Category>>(categories);

            CategoriesModel categoriesModel = new CategoriesModel();
            categoriesModel.Categories.AddRange(mappedCategories);

            return categoriesModel;
        }

        public async override Task<PostsModel> GetAllPosts(EmptyRequest request, ServerCallContext context)
        {
            var posts = await postRepository.GetAll().Include(p => p.Category).ToListAsync();

            var mappedPosts = mapper.Map<List<PostEntity>, List<Post>>(posts);

            PostsModel postsModel = new PostsModel();
            postsModel.Posts.AddRange(mappedPosts);

            return postsModel;
        }

        public async override Task<Category> GetCategoryById(GetByIdModel request, ServerCallContext context)
        {
            var category = await categoryRepository.GetByIdAsync(request.Id);

            var mappedCategory = mapper.Map<CategoryEntity, Category>(category);

            return mappedCategory;
        }
        public async override Task<Post> GetPostById(GetByIdModel request, ServerCallContext context)
        {
            var post = await postRepository.GetAllByExp(p => p.Id == request.Id).Include(p => p.Category).FirstOrDefaultAsync();

            var mappedPost = mapper.Map<PostEntity, Post>(post);

            return mappedPost;
        }

    }
}
