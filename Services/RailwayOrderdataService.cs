﻿using AutoMapper;
using Avione.Common.gRpcProtos;
using Avione.RailwayOrder.gRpcProtos;
using Avione.RailwayOrderdata.gRpcProtos;
using Avione.ServiceLayer.Services.Order.Railway;
using Grpc.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using PassengerRpc = Avione.RailwayOrder.gRpcProtos.PassengersDto;
using OrderSeatRpc = Avione.RailwayOrder.gRpcProtos.OrdersSeatsDto;
using OrderFrequentNumbersRpc = Avione.RailwayOrder.gRpcProtos.OrdersFrequentNumbersDto;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Avione.Search.gRpcServices.Base;
using Microsoft.AspNetCore.Http;
using Avione.ServiceLayer.Constants;

namespace Avione.Search.gRpcServices.Services
{
    public class RailwayOrderdataService : gRpcRailwayOrderData.gRpcRailwayOrderDataBase
    {
        private readonly IUTYOrderDataService utyOrderdataService;
        private readonly IMapper mapper;
        private readonly IHttpContextAccessor _accessor;

        public RailwayOrderdataService(IUTYOrderDataService utyOrderdataService, IHttpContextAccessor accessor, IMapper mapper)
        {
            this.utyOrderdataService = utyOrderdataService;
            this.mapper = mapper;
            this._accessor = accessor;
            this._accessor.HttpContext.Request.Headers["DeviceType"] = CommonConstants.Mobile;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

        public override Task<RailwayOrderDTO> GetOrderByOrderId(OrderIdRequest request, ServerCallContext context)
        {         
            try
            {
                var getOrderByIdResult = utyOrderdataService.GetOrderByOrderId(request.OrderId).GetAwaiter().GetResult();
                
                List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO> railwayOrderDTOs = new List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO>();
                railwayOrderDTOs.Add(getOrderByIdResult);

                RailwayOrderDTO railwayOrderDTO = new RailwayOrderDTO();
                var customizedOrder = CustomizeOrder(railwayOrderDTOs);
                if (customizedOrder != null && customizedOrder.Count != 0)
                {
                    railwayOrderDTO = customizedOrder[0];
                }
                return Task.FromResult(railwayOrderDTO);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

        public override Task<RailwayOrderDTO> GetByIdAsync(orderDataId request, ServerCallContext context)
        {          
            try
            {
                var getByIdAsyncResult = utyOrderdataService.GetByIdAsync(request.Id).GetAwaiter().GetResult();

                List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO> railwayOrderDTOs = new List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO>();
                railwayOrderDTOs.Add(getByIdAsyncResult);

                RailwayOrderDTO railwayOrderDTO = new RailwayOrderDTO();

                var customizedOrder = CustomizeOrder(railwayOrderDTOs);
                if (customizedOrder != null && customizedOrder.Count != 0)
                {
                    railwayOrderDTO = customizedOrder[0];
                }

                return Task.FromResult(railwayOrderDTO);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);

            }            
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

        public override Task<RailwayOrderCollection> GetAllAsync(EmptyRequest request, ServerCallContext context)
        {         
            try
            {
                var getAllResult = utyOrderdataService.GetAllAsync().GetAwaiter().GetResult();

                var getAllResponse = CustomizeOrder(getAllResult);

                RailwayOrderCollection railwayOrderCollection = new RailwayOrderCollection();
                
                railwayOrderCollection.RailwayOrders.AddRange(getAllResponse);

                return Task.FromResult(railwayOrderCollection);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

        public override Task<RailwayOrderCollection> GetOrdersByPageAsync(PaginationRequest request, ServerCallContext context)
        {
            try
            {
                utyOrderdataService.CurrentUserId = context.CurrentUserId();

                var getByPageResult = utyOrderdataService.GetByPageAsync(request.Page, request.PageSize).GetAwaiter().GetResult();

                var getByPageResponse = CustomizeOrder(getByPageResult);

                RailwayOrderCollection railwayOrderCollection = new RailwayOrderCollection();

                railwayOrderCollection.RailwayOrders.AddRange(getByPageResponse);

                return Task.FromResult(railwayOrderCollection);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }            
        } 

        public List<RailwayOrderDTO> CustomizeOrder (List<BusinessLogicLayer.Models.RailwayModels.RailwayOrderDTO> Orders)
        {
            if (Orders.Count == 0 || Orders == null) return new List<RailwayOrderDTO>();

            var orders = new List<RailwayOrderDTO>();

            for (int k = 0; k < Orders.Count; k++)
            {
            RailwayOrderDTO railwayOrderDTO = new RailwayOrderDTO();

            railwayOrderDTO.OrderId = Orders[k]?.OrderId ?? string.Empty;
            railwayOrderDTO.Id = Orders[k]?.Id ?? 0;
            railwayOrderDTO.OrderStatus = Orders[k]?.OrderStatus ?? string.Empty;
            railwayOrderDTO.TotalCost = Orders[k].TotalCost;
            railwayOrderDTO.UserId = Orders[k]?.UserId ?? string.Empty;
            railwayOrderDTO.CreatedTime = Orders[k]?.CreatedTime ?? string.Empty;
            railwayOrderDTO.AppUserId = Orders[k].AppUserId ?? 0;
            railwayOrderDTO.StatusFormatted = Orders[k]?.StatusFormatted ?? string.Empty;
            IList<TrainDTO> Forward = MakeTickets(Orders[k]?.TrainDetails?.Forward);
            IList<TrainDTO> Backward = MakeTickets(Orders[k]?.TrainDetails?.Backward);
            railwayOrderDTO.TrainDetails = new TrainDetailsDTO();

            railwayOrderDTO.TrainDetails.Forward.AddRange(Forward);
            railwayOrderDTO.TrainDetails.Backward.AddRange(Backward);

            railwayOrderDTO.Passengers.AddRange(ConvertPassangers(Orders[k]?.Passengers));

            List<InventoryProductItemDTO> inventoryProducts = new List<InventoryProductItemDTO>();

            for (int i = 0; i < Orders[k].InventoryProductItem.Count; i++)
            {
                inventoryProducts.Add(new InventoryProductItemDTO
                {
                    ItemId = Orders[k]?.InventoryProductItem[i]?.ItemId ?? string.Empty,
                    SerialId = Orders[k]?.InventoryProductItem[i]?.SerialId ?? string.Empty,
                    InsuranceNumber = Orders[k]?.InventoryProductItem[i]?.InsuranceNumber ?? string.Empty,
                    CreatedDateTime = Orders[k]?.InventoryProductItem[i]?.CreatedDateTime ?? string.Empty,
                    Price = Orders[k].InventoryProductItem[i].Price,
                    ProductId = Orders[k]?.InventoryProductItem[i]?.ProductId ?? string.Empty,
                    SupplierId = Orders[k]?.InventoryProductItem[i]?.SupplierId ?? string.Empty,
                    PassengerDoc = Orders[k]?.InventoryProductItem[i]?.PassengerDoc ?? string.Empty,
                    Id = Orders[k].InventoryProductItem[i].Id,
                    ExpressId = Orders[k]?.InventoryProductItem[i]?.ExpressId ?? string.Empty,
                    OrderId = Orders[k]?.InventoryProductItem[i]?.OrderId ?? string.Empty,
                });
            }

            railwayOrderDTO.InventoryProductItem.AddRange(inventoryProducts);

            orders.Add(railwayOrderDTO);
            }

            return orders;
        }

        public List<TrainDTO> MakeTickets(List<Avione.BusinessLogicLayer.Models.RailwayModels.TrainDTO> tickets)
        {
            if (tickets.Count == 0 || tickets == null) return new List<TrainDTO>();


            List<TrainDTO> Forward = new List<TrainDTO>();

            for (int i = 0; i < tickets.Count; i++)
            {
                var forwardTrain = tickets[i];

                IList<TicketDTO> ticketDTOs = new List<TicketDTO>();

                for (int j = 0; j < forwardTrain.Tickets.Count; j++)
                {
                    IList<PassengerRpc> passengersDTOs = ConvertPassangers(forwardTrain.Tickets[j].Passengers);

                    ticketDTOs.Add(new TicketDTO
                    {
                        Id = forwardTrain?.Tickets[j]?.Id ?? string.Empty,
                        CodeServ = forwardTrain?.Tickets[j]?.CodeServ ?? string.Empty,
                        D5 = forwardTrain?.Tickets[j]?.D5 ?? string.Empty,
                        ExpressId = forwardTrain?.Tickets[j]?.ExpressId ?? string.Empty,
                        ListPassIssued = forwardTrain?.Tickets[j]?.ListPassIssued ?? string.Empty,
                        NDSRate1 = forwardTrain?.Tickets[j]?.NDSRate1 ?? string.Empty,
                        NDSRate2 = forwardTrain?.Tickets[j]?.NDSRate2 ?? string.Empty,
                        NDSRate3 = forwardTrain?.Tickets[j]?.NDSRate3 ?? string.Empty,
                        Number = forwardTrain?.Tickets[j]?.Number ?? string.Empty,
                        NumberReservation = forwardTrain?.Tickets[j]?.NumberReservation ?? string.Empty,
                        PassCount = forwardTrain?.Tickets[j]?.PassCount ?? string.Empty,
                        QrCode = forwardTrain?.Tickets[j]?.QrCode ?? string.Empty,
                        RefoundTicket = forwardTrain.Tickets[j].RefoundTicket,
                        RefoundTicketInsurance = forwardTrain.Tickets[j].RefoundTicketInsurance,
                        Seats = forwardTrain?.Tickets[j]?.Seats ?? string.Empty,
                        SeatsType = forwardTrain?.Tickets[j]?.SeatsType ?? string.Empty,
                        Tariff = forwardTrain?.Tickets[j]?.Tariff ?? string.Empty,
                        TariffB = forwardTrain?.Tickets[j]?.TariffB ?? string.Empty,
                        TariffBP = forwardTrain?.Tickets[j]?.TariffBP ?? string.Empty,
                        TariffCom = forwardTrain?.Tickets[j]?.TariffCom ?? string.Empty,
                        TariffComNDS = forwardTrain?.Tickets[j]?.TariffComNDS ?? string.Empty,
                        TariffCredit = forwardTrain?.Tickets[j]?.TariffCredit ?? string.Empty,
                        TariffEuro = forwardTrain?.Tickets[j]?.TariffEuro ?? string.Empty,
                        TariffInfo = forwardTrain?.Tickets[j]?.TariffInfo ?? string.Empty,
                        TariffInsurance = forwardTrain?.Tickets[j]?.TariffInsurance ?? string.Empty,
                        TariffNDS = forwardTrain?.Tickets[j]?.TariffNDS ?? string.Empty,
                        TariffP = forwardTrain?.Tickets[j]?.TariffP ?? string.Empty,
                        TariffService = forwardTrain?.Tickets[j]?.TariffService ?? string.Empty,
                        TariffServiceNDS = forwardTrain?.Tickets[j]?.TariffServiceNDS ?? string.Empty,
                        TariffType = forwardTrain?.Tickets[j]?.TariffType ?? string.Empty,
                        TariffVed = forwardTrain?.Tickets[j]?.TariffVed ?? string.Empty,
                        TariffVedCom = forwardTrain?.Tickets[j]?.TariffVedCom ?? string.Empty,
                        TariffVedComNDS = forwardTrain?.Tickets[j]?.TariffVedComNDS ?? string.Empty,
                        TariffVedNDS = forwardTrain?.Tickets[j]?.TariffVedNDS ?? string.Empty,
                        TariffVedService = forwardTrain?.Tickets[j]?.TariffVedService ?? string.Empty,
                        TariffVedServiceNDS = forwardTrain?.Tickets[j]?.TariffVedServiceNDS ?? string.Empty,
                        VedTT = forwardTrain?.Tickets[j]?.VedTT ?? string.Empty,

                    });

                    ticketDTOs[j].Passengers.AddRange(passengersDTOs);
                }

                Forward.Add(new TrainDTO
                {
                    Identifier = forwardTrain?.Identifier ?? string.Empty,
                    ExspOrderId = forwardTrain?.ExspOrderId ?? string.Empty,
                    AdultPass = forwardTrain.AdultPass,
                    Passengers = forwardTrain.Passengers,
                    DirectionType = forwardTrain?.DirectionType ?? string.Empty,
                    CreateTime = forwardTrain?.CreateTime ?? string.Empty,
                    TrainArrivalDateTime = forwardTrain?.TrainArrivalDateTime ?? string.Empty,
                    TrainDepartureDateTim = forwardTrain?.TrainDepartureDateTime ?? string.Empty,
                    TotalCost = forwardTrain.TotalCost,
                    InsuranceAmount = forwardTrain.InsuranceAmount,
                    OrderId = forwardTrain?.OrderId ?? string.Empty,
                    RefoundOrder = forwardTrain?.RefoundOrder ?? false,
                    OrderStatus = forwardTrain?.OrderStatus ?? string.Empty,
                    TimeInWay = new TimeInWayDTO
                    {
                        Days = forwardTrain.TimeInWay.Days,
                        Hours = forwardTrain.TimeInWay.Hours,
                        Minutes = forwardTrain.TimeInWay.Minutes,
                        Seconds = forwardTrain.TimeInWay.Seconds,
                    },
                    Departure = new DepartureDTO
                    {
                        Date = forwardTrain?.Departure?.Date ?? string.Empty,
                        DiffTime = forwardTrain?.Departure?.DiffTime ?? string.Empty,
                        LocalDate = forwardTrain?.Departure?.LocalDate ?? string.Empty,
                        LocalTime = forwardTrain?.Departure?.LocalTime ?? string.Empty,
                        Station = forwardTrain?.Departure?.Station ?? string.Empty,
                        StationCode = forwardTrain?.Departure?.StationCode ?? string.Empty,
                        Time = forwardTrain?.Departure?.Time ?? string.Empty,
                        Train = forwardTrain?.Departure?.Train ?? string.Empty,
                        TrainCat = forwardTrain?.Departure?.TrainCat ?? string.Empty,
                        TrainType = forwardTrain?.Departure?.TrainType ?? string.Empty,
                    },
                    Arrival = new DepartureDTO
                    {
                        Date = forwardTrain?.Arrival?.Date ?? string.Empty,
                        DiffTime = forwardTrain?.Arrival?.DiffTime ?? string.Empty,
                        LocalDate = forwardTrain?.Arrival?.LocalDate ?? string.Empty,
                        LocalTime = forwardTrain?.Arrival?.LocalTime ?? string.Empty,
                        Station = forwardTrain?.Arrival?.Station ?? string.Empty,
                        StationCode = forwardTrain?.Arrival?.StationCode ?? string.Empty,
                        Time = forwardTrain?.Arrival?.Time ?? string.Empty,
                        Train = forwardTrain?.Arrival?.Train ?? string.Empty,
                        TrainCat = forwardTrain?.Arrival?.TrainCat ?? string.Empty,
                        TrainType = forwardTrain?.Arrival?.TrainType ?? string.Empty,
                    },
                    Car = new CarDTO
                    {
                        Type = forwardTrain?.Car?.Type ?? string.Empty,
                        AddSigns = forwardTrain?.Car?.AddSigns ?? string.Empty,
                        CarrierName = forwardTrain?.Car?.CarrierName ?? string.Empty,
                        ClassServiceType = forwardTrain?.Car?.ClassServiceType ?? string.Empty,
                        Number = forwardTrain?.Car?.Number ?? string.Empty,
                        Owner = forwardTrain?.Car?.Owner ?? string.Empty,
                        TripClass = forwardTrain?.Car?.TripClass ?? string.Empty,
                    }

                });
                Forward[i].Tickets.AddRange(ticketDTOs);
            }

            return Forward;
        }

        public List<PassengerRpc> ConvertPassangers(List<PassengersDto> passengers)
        {
            if (passengers.Count == 0) return new List<PassengerRpc>();
            
            List<PassengerRpc> passengersDTOs = new List<PassengerRpc>();

            for (int n = 0; n < passengers.Count; n++)
            {
                var trainPassengers = passengers;

                IList<OrderSeatRpc> orderSeats = new List<OrderSeatRpc>();

                IList<OrderFrequentNumbersRpc> ordersFrequentNumbers = new List<OrderFrequentNumbersRpc>();

                foreach (OrdersSeatsDto orderSeat in trainPassengers[n]?.Seats ?? new List<OrdersSeatsDto>())
                {
                    orderSeats.Add(new OrderSeatRpc
                    {
                        FarId = orderSeat?.FarId ?? 0,
                        Seat = orderSeat?.Seat ?? string.Empty,
                    });
                }

                foreach (var orderFrequentNum in trainPassengers[n]?.FrequentNumbers ?? new List<OrdersFrequentNumbersDto>())
                {
                    ordersFrequentNumbers.Add(new OrderFrequentNumbersRpc
                    {
                        Airline = orderFrequentNum?.Airline ?? string.Empty,
                        Number = orderFrequentNum?.Number ?? string.Empty,
                    });
                }

                passengersDTOs.Add(new PassengerRpc
                {
                    Id = trainPassengers[n].Id,
                    AppPassengerId = trainPassengers[n].AppPassengerId,
                    AppUserId = trainPassengers[n].AppUserId ?? 0,
                    BirthDate = trainPassengers[n]?.BirthDate ?? string.Empty,
                    FirstName = trainPassengers[n]?.FirstName ?? string.Empty,
                    LastName = trainPassengers[n]?.LastName ?? string.Empty,
                    MiddleName = trainPassengers[n]?.MiddleName ?? string.Empty,
                    PassengerShortInfo = trainPassengers[n]?.PassengerShortInfo ?? string.Empty,
                    ParentDocNumber = trainPassengers[n]?.ParentDocNumber ?? string.Empty,
                    PhoneNumber = trainPassengers[n]?.PhoneNumber ?? string.Empty,
                    PhoneNumberInfo = new RailwayOrder.gRpcProtos.CountryDto
                    {
                        CitizenshipCode = trainPassengers[n]?.PhoneNumberInfo?.CitizenshipCode ?? string.Empty,
                        Code = trainPassengers[n]?.PhoneNumberInfo?.Code ?? string.Empty,
                        CodeRus = trainPassengers[n]?.PhoneNumberInfo?.CodeRus ?? string.Empty,
                        IsDefault = trainPassengers[n]?.PhoneNumberInfo?.IsDefault ?? false,
                        Locale = trainPassengers[n]?.PhoneNumberInfo?.Locale ?? string.Empty,
                        Name = trainPassengers[n]?.PhoneNumberInfo?.Name ?? string.Empty,
                        Phone = trainPassengers[n]?.PhoneNumberInfo?.Phone ?? string.Empty,
                    },
                    Email = trainPassengers[n]?.Email ?? string.Empty,
                    Type = new RailwayOrder.gRpcProtos.PassengerTypeDto
                    {
                        Code = trainPassengers[n]?.Type?.Code ?? string.Empty,
                        Description = trainPassengers[n]?.Type?.Description ?? string.Empty,
                        Name = trainPassengers[n]?.Type?.Name ?? string.Empty,
                        IsDefault = trainPassengers[n]?.Type?.IsDefault ?? false,
                        OrderNumber = trainPassengers[n]?.Type?.OrderNumber ?? 0,

                    },
                    Gender = new RailwayOrder.gRpcProtos.GenderDto
                    {
                        OrderNumber = trainPassengers[n]?.Gender?.OrderNumber ?? 0,
                        Code = trainPassengers[n]?.Gender?.Code ?? string.Empty,
                        Name = trainPassengers[n]?.Gender?.Name ?? string.Empty,
                        IsDefault = trainPassengers[n]?.Gender?.IsDefault ?? false,
                    },
                    Citizenship = new RailwayOrder.gRpcProtos.CountryDto
                    {
                        IsDefault = trainPassengers[n]?.Citizenship?.IsDefault ?? false,
                        CitizenshipCode = trainPassengers[n]?.Citizenship?.Code ?? string.Empty,
                        Code = trainPassengers[n]?.Citizenship?.Code ?? string.Empty,
                        Name = trainPassengers[n]?.Citizenship?.Name ?? string.Empty,
                        CodeRus = trainPassengers[n]?.Citizenship?.CodeRus ?? string.Empty,
                        Locale = trainPassengers[n]?.Citizenship?.Locale ?? string.Empty,
                        Phone = trainPassengers[n]?.Citizenship?.Phone ?? string.Empty,
                    },
                    Document = new RailwayOrder.gRpcProtos.DocumentDto
                    {
                        Expire = trainPassengers[n]?.Document?.Expire ?? string.Empty,
                        Issue = trainPassengers[n]?.Document?.Issue ?? string.Empty,
                        Number = trainPassengers[n]?.Document?.Number ?? string.Empty,
                        Type = new RailwayOrder.gRpcProtos.DocumentTypeDto
                        {
                            Code = trainPassengers[n]?.Document?.Type?.Code ?? string.Empty,
                            Id = trainPassengers[n]?.Document?.Type?.Id ?? 0,
                            IsDefault = trainPassengers[n]?.Document?.Type?.IsDefault ?? false,
                            Name = trainPassengers[n]?.Document?.Type?.Name ?? string.Empty,
                            Name1 = trainPassengers[n]?.Document?.Type?.Name1 ?? string.Empty,
                            OrderNumber = trainPassengers[n]?.Document?.Type?.OrderNumber ?? 0
                        }
                    }
                });

                passengersDTOs[n].FrequentNumbers.AddRange(ordersFrequentNumbers);
                passengersDTOs[n].Seats.AddRange(orderSeats);
            }
            return passengersDTOs;
        }
    }
}
