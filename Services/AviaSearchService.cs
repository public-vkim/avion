﻿using AutoMapper;
using Avione.BizLogicLayer.Flight.Enums;
using Avione.Common.gRpcProtos;
using Avione.BizLogicLayer.Flight.Models;
using Avione.Search.gRpcProtos;
using Avione.ServiceLayer;
using Avione.ServiceLayer.Services.OrderFactory;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Avione.Search.gRpcServices.Base;
using Avione.ServiceLayer.Constants;
using Microsoft.AspNetCore.Http;

namespace Avione.SearchApi.Services
{
    public class AviaSearchService : AviaSearch.AviaSearchBase
    {
        private readonly IMapper _mapper;
        private readonly ILogger<AviaSearchService> _logger;
        private readonly ISearchFactory _searchFactory;
        private readonly IHttpContextAccessor _accessor;
        public AviaSearchService
        (
            IMapper mapper, 
            ILogger<AviaSearchService> logger, 
            ISearchFactory searchFactory,
            IHttpContextAccessor accessor
        )
        {
            this._logger = logger;
            this._mapper = mapper;
            _searchFactory = searchFactory;
            _accessor = accessor;
            _accessor.HttpContext.Request.Headers["DeviceType"] = CommonConstants.Mobile;
        }

        public override async Task<SearchResult> Search(SearchRequest request, ServerCallContext context)
        {
            try
            {
                var watch = Stopwatch.StartNew();

                var response = new SearchResult();
                var mapped = _mapper.Map<SearchRequest, SearchRequestDto>(request);
                if (mapped == null)
                    throw new Exception("Body cannot mapped");


                var flightSearchService = _searchFactory.GetService(TicketOwner.ETM);

                var result = await flightSearchService.Search(mapped);

                var carriers = _mapper.Map<List<CarrierDto>, List<Carrier>>(result.Carriers);
                response.Carriers.AddRange(carriers);
                response.Session = result.Session;

                watch.Stop();

                log($"::::: SEARCH Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms, Count: {carriers.Count}, [Search] {DateTime.Now.ToLongDateString()} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new SearchResult();
            }
        }



        public override async Task<SearchResult> SearchTickets(SearchRequest request, ServerCallContext context)
        {
            try
            {
                var watch = Stopwatch.StartNew();
                string deviceName = context.CurrentDevice();

                var response = new SearchResult();
                var mapped = _mapper.Map<SearchRequest, SearchRequestDto>(request);
                if (mapped == null)
                    throw new Exception("Body cannot mapped");


                var flightSearchService = _searchFactory.GetService(TicketOwner.ETM);
                //flightSearchService.CurrentUserId = context.TryGetCurrentUserId();

                var result = await flightSearchService.SearchTickets(mapped, deviceName);

                var carriers = _mapper.Map<List<CarrierDto>, List<Carrier>>(result.Carriers);
                response.Carriers.AddRange(carriers);
                response.Session = result?.Session ?? String.Empty;
                response.RequestId = result.RequestId;
                response.Status = result.Status;

                watch.Stop();

                log($"::::: SEARCH Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms, Count: {carriers.Count}, [Search] {DateTime.Now.ToLongDateString()} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new SearchResult();
            }
        }


        public override async Task<SearchResult> SearchTicketsContinue(SearchRequestContinue request, ServerCallContext context)
        {
            try
            {
                var watch = Stopwatch.StartNew();

                if (request == null)
                    throw new Exception("Body cannot mapped");

                var flightSearchService = _searchFactory.GetService(TicketOwner.ETM);
                //flightSearchService.CurrentUserId = context.TryGetCurrentUserId();
                //log($"current user {flightSearchService.CurrentUserId}");
                var result = await flightSearchService.SearchTicketsContinue(request.RequestId);

                var carriers = _mapper.Map<List<CarrierDto>, List<Carrier>>(result.Carriers);
                var response = new SearchResult();
                response.Carriers.AddRange(carriers);
                response.RequestId = result.RequestId;
                response.Status = result.Status;

                watch.Stop();

                log($"::::: SEARCH CONTINUE Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms, Count: {carriers.Count}, [Search] {DateTime.Now.ToLongDateString()} :::::");

                return response;
            }
            catch (Exception ex)
            {

                _logger.LogError(ex.Message);
                return new SearchResult();
            }
        }
             

        public override async Task<SearchResult> SearchCharterTickets(EmptyRequest request, ServerCallContext context)
        {
            try
            {
                var watch = Stopwatch.StartNew();

                var response = new SearchResult();

                var searchService = _searchFactory.GetService(TicketOwner.CHARTER_AGENT);

                var result = await searchService.Search(new SearchRequestDto());

                var carriers = _mapper.Map<List<CarrierDto>, List<Carrier>>(result.Carriers);
                response.Carriers.AddRange(carriers);
                response.Session = result.Session;

                watch.Stop();

                log($"::::: SEARCH Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms, Count: {carriers.Count}, [Search] {DateTime.Now.ToLongDateString()} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new SearchResult();
            }
        }

        private void log(string message)
        {
            _logger.LogInformation($"\n\t::::: {message} :::::\n\t");
        }
    }
}
