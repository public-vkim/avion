﻿using AutoMapper;
using Avione.BizLogicLayer.Flight.Enums;
using Avione.BizLogicLayer.Flight.Services;
using Avione.Common.gRpcProtos;
using Avione.BizLogicLayer.Flight.Models;
using Avione.BizLogicLayer.Flight.Services;
using Avione.Search.gRpcProtos;
using Avione.ServiceLayer;
using Avione.ServiceLayer.Models;
using Avione.ServiceLayer.Models.Output;
using Avione.ServiceLayer.Models.Inputs;
using Avione.ServiceLayer.Services.Order;
using Avione.ServiceLayer.Services.OrderFactory;
using ETM.Reference;
using Grpc.Core;
using IO.Swagger.Model;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Avione.SearchApi.Services
{
    public class OfferService : ETMOffer.ETMOfferBase
    {
        private readonly IMapper _mapper;
        private readonly ILogger<OfferService> _logger;
        private readonly IOrderFactoryCreator _orderFactoryCreator;

        public OfferService(IMapper mapper, ILogger<OfferService> logger, IOrderFactoryCreator orderFactoryCreator)
        {
            _mapper = mapper;
            _logger = logger;
            _orderFactoryCreator = orderFactoryCreator;
        }

        public override async Task<OfferAvailabilityResponse> GetOfferAvailability(OfferAvailabilityRequest request, ServerCallContext context)
        {
            try
            {
                log($"::::: GetOfferAvailability BuyId: { request.BuyId }");

                string ticketOwner = request.TicketOwner;

                if (request.BuyId.StartsWith("vs", StringComparison.CurrentCultureIgnoreCase))
                {
                    ticketOwner = TicketOwner.VIP_SERVICE;
                }
                else if (!request.BuyId.StartsWith("vs") && !request.BuyId.StartsWith("eb") && !request.BuyId.StartsWith("da"))
                {
                    ticketOwner = TicketOwner.CHARTER_AGENT;
                }

                var model = new OfferAvailabilityInput() { TicketOwner = ticketOwner, BuyId = request.BuyId, RequiredTicketQty = 1 };

                var offerService = (IOrderAvailabilityStrategy)_orderFactoryCreator.GetOrderFactory(ticketOwner).OrderService();

                OfferAvailabilityData result = await offerService.GetOfferAvailability(model);

                var offer = _mapper.Map<OfferAvailabilityData, OfferAvailabilityClient>(result);

                OfferAvailabilityResponse response = new OfferAvailabilityResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = offer
                };

                log($"::::: GetOfferAvailability { offerService.GetType().Name } result: {JsonConvert.SerializeObject(response)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new OfferAvailabilityResponse()
                {
                    Status = ResponseStatusType.Error,
                    Data = new OfferAvailabilityClient()
                    {
                        Message = "Предложение более не доступно, попробуйте другие варианты маршрута."
                    }
                };
            }
        }

        public override Task<FareFamilyResponse> GetFareFamily(FareFamilyRequest request, ServerCallContext context)
        {
            try
            {
                var watch = Stopwatch.StartNew();

                log($"::::: GetFareFamily BuyId: { request.BuyId }");

                string ticketOwner = TicketOwner.ETM;

                if (request.BuyId.StartsWith("vs", StringComparison.CurrentCultureIgnoreCase))
                {
                    ticketOwner = TicketOwner.VIP_SERVICE;
                }
                else if (!request.BuyId.StartsWith("vs") && !request.BuyId.StartsWith("eb") && !request.BuyId.StartsWith("da"))
                {
                    ticketOwner = TicketOwner.CHARTER_AGENT;
                }

                var tariffService = (IOrderTariffStrategy)_orderFactoryCreator.GetOrderFactory(ticketOwner).OrderService();

                var result = tariffService.GetOfferFareFamily(new FareFamilyInput() { BuyId = request.BuyId, Session = request.Session, RequestId = request.RequestId });

                FareFamilyResponse response = new FareFamilyResponse();
                if (result.Status == StatusEnum.Ok)
                {
                    var fareFamily = _mapper.Map<FareFamilyDto, FareFamilyClient>(result);
                    response.Status = ResponseStatusType.Ok;
                    response.Data = fareFamily;
                }
                else
                {
                    response.Status = ResponseStatusType.Error;
                    response.Message = result.Message;
                    response.Data = new FareFamilyClient() { Message = "" };
                }

                watch.Stop();

                _logger.LogInformation($":::::: EtmOfferService.GetFareFamily: {JsonConvert.SerializeObject(response)} ::::::");

                log($"::::: GetFareFamily Response Time: {watch.ElapsedMilliseconds / 1000}sec OR {watch.ElapsedMilliseconds}ms, Status: {response.Status}, [Search] {DateTime.Now.ToLongDateString()}");

                return Task.FromResult(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return Task.FromResult(new FareFamilyResponse()
                {
                    Status = ResponseStatusType.Error,
                    Data = new FareFamilyClient()
                    {
                        Message = "Не удалось найти тариф на этот билет."
                    }
                });
            }
        }

        private void log(string message)
        {
            _logger.LogInformation($"\n\t::::: {message} :::::\n\t");
        }
    }
}
