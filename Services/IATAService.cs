﻿using AutoMapper;
using Avione.BusinessLogicLayer.Models.FlightLocationModel;
using Avione.Infrastructure.DbContext.Context;
using Avione.Infrastructure.DbContext.Entities;
using Avione.Search.gRpcProtos;
using Avione.ServiceLayer.Services;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Avione.SearchApi.Services
{
    public class IATAService : IATA.IATABase
    {
        private readonly AppDataContext _context;
        private readonly ILogger<IATAService> _logger;
        private readonly ILocationESSearchService _locationESSearchService;

        public IATAService(AppDataContext context, ILogger<IATAService> logger, ILocationESSearchService locationESSearchService)
        {
            this._context = context;
            this._logger = logger;
            this._locationESSearchService = locationESSearchService;
        }

        public override Task<IATAResponse> GetLocation(IATARequest request, ServerCallContext context)
        {         
            
            var watch = Stopwatch.StartNew();

            string locale = "RU";
            string location = request.Filter.Trim();
            string codeUpperCase = request.Filter.Trim().ToUpper();

            _logger.LogInformation($"Location: {location}");

            var cities = _context.Cities
                             .Where(w => (w.Name.StartsWith(location) || w.Code.StartsWith(codeUpperCase)) && w.Locale == locale)
                             .OrderBy(o=> o.Name)
                             .Include(i => i.Airports)

                             .Take(5)
                             .Select(s => s).AsNoTracking().ToList();

            var airports = _context.Airports

                                .Where(w => (w.Name.StartsWith(location) || w.Code.StartsWith(codeUpperCase)) && w.Locale == locale)
                                .Take(10)
                                .Select(s => s).AsNoTracking().ToList();

            IATAResponse response = new IATAResponse();

            List<Airport> existingAirports = new List<Airport>();
            if (cities != null)
            {
                foreach (var city in cities)
                {

                    var item = new IATAResponse.Types.City()
                    {
                        Code = city.Code,
                        Name = city.Name,
                        Locale = city.Locale
                    };
                    item.Airports.AddRange(city.Airports.Select(s => new IATAResponse.Types.Airport()
                    {
                        Code = s.Code,
                        Name = s.Name,
                        Locale = s.Locale,
                        City = new IATAResponse.Types.City() { }
                    }));

                    response.Cities.Add(item);

                    existingAirports.AddRange(city.Airports);
                }
            }

            if (airports != null)
            {
                var existingAirportsIds = existingAirports.Select(s => s.Id).ToList();
                var newAirports = airports.Where(w => !existingAirportsIds.Contains(w.Id)).Select(s => s);

                response.Airports.AddRange(newAirports
                    .Select(s => new IATAResponse.Types.Airport()
                    {
                        Code = s.Code,
                        Name = s.Name,
                        Locale = s.Locale
                    }));
            }

            watch.Stop();

            _logger.LogInformation($"Response Time: {watch.ElapsedMilliseconds}ms [GetLocation]");
            _logger.LogInformation($"::::: Result: {JsonConvert.SerializeObject(response)} :::::");

            return Task.FromResult(response);
           
        }

    
        public override Task<IATAResponse> GetAirportCitiesFromES(IATARequest request, ServerCallContext context)
        {

            var result =  _locationESSearchService.GetESLocation(request.Filter.Trim()).Result;

            var cities = result.cities;
            var airports = result.airports;

            IATAResponse response = new IATAResponse();

            if (cities != null)
            {
                foreach (var city in cities)
                {
                    var item = new IATAResponse.Types.City()
                    {
                        Code = city.code,
                        Name = city.name,
                        Locale = city.locate
                    };
                    item.Airports.AddRange(city.airports.Select(s => new IATAResponse.Types.Airport()
                    {
                        Code = s.code,
                        Name = s.name,
                        Locale = s.locate,
                        City = new IATAResponse.Types.City() { }
                    }));

                    response.Cities.Add(item);
                }
            }

            if (airports != null)
            {
                var newAirports = airports;

                response.Airports.AddRange(newAirports
                    .Select(s => new IATAResponse.Types.Airport()
                    {
                        Code = s.code,
                        Name = s.name,
                        Locale = s.locate,
                        City = new IATAResponse.Types.City
                        {
                            Name = s.city
                        }
                    }));
            }

            return Task.FromResult(response);         
       }
    }
}
