﻿using AutoMapper;
using Avione.BizLogicLayer.Flight.Enums;
using Avione.BizLogicLayer.Flight.Models.PaymentModel;
using Avione.Common.gRpcProtos;
using Avione.Order.gRpcProtos;
using Avione.Search.gRpcServices.Base;
using Avione.ServiceLayer.Constants;
using Avione.ServiceLayer.Models.ViewModels;
using Avione.ServiceLayer.Services;
using Avione.User.gRpcProtos;
using Grpc.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Avione.SearchApi.Services
{
    public class AvioneUserService : gRpcUserService.gRpcUserServiceBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;
        private readonly ILogger<AvioneUserService> _logger;
        private readonly IApelsinService _apelsinService;
        private readonly IHttpContextAccessor _accessor;

        public AvioneUserService(IMapper mapper,
            IUserService userService,
            IAuthService authService,
            ILogger<AvioneUserService> logger,
            IApelsinService apelsinService,
             IHttpContextAccessor accessor)
        {
            _logger = logger;
            _apelsinService = apelsinService;
            _mapper = mapper;
            _userService = userService;
            _authService = authService;
            _accessor = accessor;
            _accessor.HttpContext.Request.Headers["DeviceType"] = CommonConstants.Mobile;
        }

        public override async Task<SendMeCodeResponse> SendMeCode(SendMeCodeRequest request, ServerCallContext context)
        {
            try
            {
                log("::::: SEND_ME_CODE :::::");

                var watch = Stopwatch.StartNew();

                var requestViewModel = _mapper.Map<SendMeCodeRequest, SendMeCodeInput>(request);
                if (requestViewModel == null)
                    throw new Exception("Body cannot mapped");

                log($"::::: Request: {requestViewModel.ToJson()} :::::");

                if (string.IsNullOrEmpty(requestViewModel.PhoneNumber) || string.IsNullOrEmpty(requestViewModel.PhoneNumberCode))
                    throw new Exception("Номер телефона и код страны не указаны");

                var result = await _authService.SendMeCode(requestViewModel);

                SendMeCodeResponse response = new SendMeCodeResponse();
                if (result.Status == ServiceLayer.Enums.ResponseStatusType.Ok)
                {
                    response.Status = ResponseStatusType.Ok;
                }
                else
                {
                    response.Status = ResponseStatusType.Error;
                    response.Message = result.Message;
                }

                watch.Stop();

                log($"::::: SEND_ME_CODE Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: Response: {response} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                if (ex.InnerException != null)
                    log($"::::: GetPassengers InnerException Error: {ex.InnerException.Message} :::::");
                return new SendMeCodeResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        public override async Task<VerifyPhoneNumberResponse> VerifyMe(VerifyPhoneNumberRequest request, ServerCallContext context)
        {
            try
            {
                log("::::: VERIFY_ME :::::");
                var watch = Stopwatch.StartNew();

                var requestViewModel = _mapper.Map<VerifyPhoneNumberRequest, VerifyPhoneNumberInput>(request);
                if (requestViewModel == null)
                    throw new Exception("Body cannot mapped");

                log($"::::: Request: {requestViewModel.ToJson()} :::::");

                if (string.IsNullOrEmpty(requestViewModel.PhoneNumber) || string.IsNullOrEmpty(requestViewModel.PhoneNumberCode))
                    throw new Exception("Номер телефона и код страны не указаны");
                if (string.IsNullOrEmpty(requestViewModel.SMSCode))
                    throw new Exception("СМС код требуется");
                //if (string.IsNullOrEmpty(mapped.Token))
                //    throw new Exception("Push Token требуется");

                var result = await _authService.VerifyMe(requestViewModel);

                VerifyPhoneNumberResponse response = new VerifyPhoneNumberResponse();
                if (result.Status == ServiceLayer.Enums.ResponseStatusType.Ok)
                {
                    response.Status = ResponseStatusType.Ok;
                    response.Data = new VerifyPhoneNumberClient()
                    {
                        Verified = result.Data.Verified,
                        Attempts = result.Data.Attempts,
                        Token = _mapper.Map<LoginTokenClient>(result.Data.Token)
                    };
                }
                else
                {
                    response.Status = ResponseStatusType.Error;
                    response.Message = result.Message;
                }

                watch.Stop();

                log($"::::: VERIFY_ME Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: Response: {response} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                if (ex.InnerException != null)
                    log($"::::: GetPassengers InnerException Error: {ex.InnerException.Message} :::::");
                return new VerifyPhoneNumberResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        public override async Task<RegisterResponse> Register(RegisterRequest request, ServerCallContext context)
        {
            try
            {
                log("::::: REGISTER USER :::::");

                var watch = Stopwatch.StartNew();

                var mapped = _mapper.Map<RegisterRequest, RegisterInput>(request);
                if (mapped == null)
                    throw new Exception("Body cannot mapped");

                log($"::::: Request: {mapped.ToJson()} :::::");

                if (string.IsNullOrEmpty(mapped.PhoneNumber))
                    throw new Exception("Номер телефона обязателен");

                if (mapped.PhoneNumberInfo == null || string.IsNullOrEmpty(mapped.PhoneNumberInfo.Code))
                    throw new Exception("Код номера телефона не указан");
                if (string.IsNullOrEmpty(mapped.FirstName))
                    throw new Exception("Имя обязательно");
                if (string.IsNullOrEmpty(mapped.LastName))
                    throw new Exception("Фамилия обязательна");
                if (string.IsNullOrEmpty(mapped.MiddleName))
                    throw new Exception("Требуется отчество");
                if (mapped.Gender == null || string.IsNullOrEmpty(mapped.Gender.Code))
                    throw new Exception("Пол обязательно");
                if (string.IsNullOrEmpty(mapped.DateOfBirth))
                    throw new Exception("День рождения обязателен");

                var result = await _userService.Register(mapped);

                var data = _mapper.Map<UserData, UserClient>(result);
                RegisterResponse response = new RegisterResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = data
                };

                watch.Stop();

                log($"::::: REGISTER USER Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: Response: {response} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new RegisterResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<UserDetailsResponse> Get(EmptyRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();
                log($"::::: GET USER, UserId: {currentUserId} :::::");

                var watch = Stopwatch.StartNew();

                var result = await _userService.Get(context.CurrentUserId());

                var data = _mapper.Map<UserData, UserClient>(result);
                UserDetailsResponse response = new UserDetailsResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = data
                };

                watch.Stop();

                log($"::::: GET USER Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: Response: {response} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                if (ex.InnerException != null)
                    log($"::::: GetPassengers InnerException Error: {ex.InnerException.Message} :::::");
                return new UserDetailsResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<UserBalanceResponse> GetBalance(EmptyRequest request, ServerCallContext context)
        {
            int currentUserId = context.CurrentUserId();

            log($"::::: GetBalance, UserId: {currentUserId} :::::");

            var userBalance = await _userService.GetUserBalance(currentUserId);

            return new UserBalanceResponse
            {
                Amount = userBalance.BalanceAmount.ToString(),
                AmountFormatted = userBalance.BalanceAmountFormatted

            };
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<PaymentDataResponse> GeneratePaymentLink(PaymentLinkRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: GeneratePaymentLink, UserId: {currentUserId} :::::");

                var watch = Stopwatch.StartNew();


                PaymentDataBodyDto result = await _userService.GeneratePaymentLink(currentUserId, (int)decimal.Parse(request.Amount));

                if (result.Status == StatusEnum.Error)
                    return new PaymentDataResponse { Status = ResponseStatusType.Error, Message = result.Message };


                var paymentData = new PaymentDataClient
                {
                    Payload = new PayloadDataClient
                    {
                        Result = result.Data.SucessRedirectLink
                    },
                    Status = "redirect"
                };

                PaymentDataResponse response = new PaymentDataResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = paymentData
                };

                watch.Stop();

                log($"::::: GeneratePaymentLink Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: GeneratePaymentLink Result: {JsonConvert.SerializeObject(response)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new PaymentDataResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<PaymentFromUserBalanceResponse> MakePaymentFromUserBalance(PaymentFromUserBalanceRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: MAKE PAYMENT FROM USER BALANCE, UserId: {currentUserId} :::::");

                var watch = Stopwatch.StartNew();

                var result = await _apelsinService.MakeOrderPayment(new ServiceLayer.Models.Apelsin.ApelsinTransactionRequest
                {
                    OrderId = request.OrderId,
                    Amount = 0,
                    UserId = currentUserId,
                    TransactionId = 0
                });

                watch.Stop();

                log($"::::: GET USER Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");

                PaymentFromUserBalanceResponse response = new PaymentFromUserBalanceResponse();


                if (result.Status == BizLogicLayer.Flight.Models.PaymentDataDto.PaymentStatus.Ok)
                {
                    response.Status = ResponseStatusType.Ok;
                    response.Message = "Success";
                }
                else
                {
                    response.Status = ResponseStatusType.Error;
                    response.Message = result?.Message ?? "Error in making payment from user balance.";
                }

                log($"::::: Response: {response} :::::");

                return response;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                if (ex.InnerException != null)
                    log($"::::: GetPassengers InnerException Error: {ex.InnerException.Message} :::::");
                return new PaymentFromUserBalanceResponse
                {
                    Status = ResponseStatusType.Error,
                    Message = ex.Message
                };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<UserDetailsResponse> Edit(UserEditRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: EDIT USER, UserId: {currentUserId} :::::");

                var watch = Stopwatch.StartNew();

                var currentUser = await _userService.Get(context.CurrentUserId());
                if (currentUser == null)
                    throw new Exception("Пользователь не найден.");

                var mapped = _mapper.Map<UserClient, UserInput>(request.User);
                mapped.Id = currentUserId;

                log($"::::: Request: {mapped.ToJson()} :::::");

                var result = await _userService.Edit(mapped);

                var data = _mapper.Map<UserData, UserClient>(result);
                UserDetailsResponse response = new UserDetailsResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = data
                };

                watch.Stop();

                log($"::::: EDIT USER Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: Response: {response} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                if (ex.InnerException != null)
                    log($"::::: GetPassengers InnerException Error: {ex.InnerException.Message} :::::");
                return new UserDetailsResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }
        public override async Task<DeleteAccountResponse> DeleteAccount(DeleteAccountRequest request, ServerCallContext context)
        {

            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: DELETE USER ACCOUNT , UserId: {currentUserId} :::::");

                var currentUser = await _userService.Get(context.CurrentUserId());
                if (currentUser == null)
                    throw new Exception("Пользователь не найден.");

                var deletedUser = await _userService.DeleteAccount(currentUserId, request.PhoneNumber);
                if (deletedUser.IsDeleted)
                    return new DeleteAccountResponse() { Status = ResponseStatusType.Ok, IsDeleted = true };

                return new DeleteAccountResponse() { Status = ResponseStatusType.Error, IsDeleted = false };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                if (ex.InnerException != null)
                    log($"::::: Delete Account InnerException Error: {ex.InnerException.Message} :::::");
                return new DeleteAccountResponse() { Status = ResponseStatusType.Error, IsDeleted = false };
            }

        }
        private void log(string message)
        {
            _logger.LogInformation($"\n\t::::: {message} :::::\n\t");
        }
    }
}
