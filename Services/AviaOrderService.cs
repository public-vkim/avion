﻿using AutoMapper;
using Avione.BizLogicLayer.Flight.Enums;
using Avione.BizLogicLayer.Flight.Models;
using Avione.BizLogicLayer.Flight.Models.PaymentModel;
using Avione.Common.gRpcProtos;
using Avione.Order.gRpcProtos;
using Avione.Search.gRpcServices.Base;
using Avione.ServiceLayer.Constants;
using Avione.ServiceLayer.Models;
using Avione.ServiceLayer.Models.Inputs;
using Avione.ServiceLayer.Services;
using Avione.ServiceLayer.Services.Order;
using Avione.ServiceLayer.Services.OrderFactory;
using Grpc.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Avione.SearchApi.Services
{
    public class AviaOrderService : gRpcAviaOrder.gRpcAviaOrderBase
    {
        private readonly IMapper _mapper;
        private readonly IPassengerService _passengerService;
        private readonly ILogger<AviaOrderService> _logger;
        private readonly IOrderFactoryCreator _orderFactoryCreator;
        private readonly IOrderDataService _orderDataService;
        private readonly IHttpContextAccessor _accessor;

        public AviaOrderService
        (
            IMapper mapper,
            ILogger<AviaOrderService> logger,
            IPassengerService passengerService,
            IOrderFactoryCreator orderFactoryCreator,
             IHttpContextAccessor accessor,
            IOrderDataService orderDataService
        )
        {
            this._logger = logger;
            this._mapper = mapper;
            this._passengerService = passengerService;
            this._orderFactoryCreator = orderFactoryCreator;
            this._orderDataService = orderDataService;
            _accessor = accessor;
            _accessor.HttpContext.Request.Headers["DeviceType"] = CommonConstants.Mobile;
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<OrderResponse> CreateOrder(OrderRequest request, ServerCallContext context)
        {
            try
            {
                int? currentUserId = context.CurrentUserId();

                if (request.BuyId.StartsWith("vs", StringComparison.CurrentCultureIgnoreCase))
                {
                    request.TicketOwner = TicketOwner.VIP_SERVICE;
                }
                else if (!request.BuyId.StartsWith("vs") && !request.BuyId.StartsWith("eb") && !request.BuyId.StartsWith("da"))
                {
                    request.TicketOwner = TicketOwner.CHARTER_AGENT;
                }

                log($"::::: CREATE ORDER, UserId: {currentUserId} :::::");
                log($"::::: Request: {JsonConvert.SerializeObject(request)} :::::");

                var watch = Stopwatch.StartNew();

                var model = _mapper.Map<OrderRequest, OrderRequestDto>(request);

                model.AppUserId = currentUserId;

                var orderService = _orderFactoryCreator.GetOrderFactory(model.TicketOwner).OrderService();

                OrderResponseDto orderResponseDto = await orderService.CreateOrder(model);

                log($"::::: Order {orderService.GetType().Name} result: {JsonConvert.SerializeObject(orderResponseDto)} :::::");

                OrderResponse response = new OrderResponse() { Status = ResponseStatusType.Error };
                if (orderResponseDto.Status == StatusEnum.Ok)
                {
                    response.Status = ResponseStatusType.Ok;
                    response.Message = orderResponseDto.Message;
                    response.Data.Add(_mapper.Map<OrderDataDto, OrderDataClient>(orderResponseDto.Data));
                }

                watch.Stop();

                log($"::::: CREATE ORDER Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: Response: {JsonConvert.SerializeObject(response)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERROR CREATE ORDER: {ex.Message}");
                if (ex.InnerException != null)
                    _logger.LogError($"InnerException CREATE ORDER: {ex.InnerException.Message}");

                return new OrderResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<PaymentDataResponse> GetPaymentData(PaymentDataRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: GetPaymentData, UserId: {currentUserId} :::::");

                var watch = Stopwatch.StartNew();

                var existingOrder = await _orderDataService.GetOrderById(request.OrderId);
                if (existingOrder.AppUserId != currentUserId)
                    throw new Exception("У вас нет такого заказа билетов.");

                var model = _mapper.Map<PaymentDataRequest, FlightPaymentDataInput>(request);
                model.BuyId = existingOrder.BuyId;
                model.AppUserId = currentUserId;

                var paymentService = _orderFactoryCreator.GetOrderFactory(existingOrder.TicketOwner).OrderPaymentService();

                PaymentDataBodyDto result = await paymentService.GetPaymentData(model);

                if (result.Status == StatusEnum.Error)
                    return new PaymentDataResponse() { Status = ResponseStatusType.Error, Message = result.Message };

                log($"::::: GetPaymentData {paymentService.GetType().Name} :::::");

                // Changed...
                //var paymentData = _mapper.Map<PaymentBody, PaymentBodyResponse>(result.Data);

                // Now Only SuccessRedirectLink is coming that we need ...
                var paymentData = new PaymentDataClient
                {
                    Payload = new PayloadDataClient
                    {
                        Result = result.Data.SucessRedirectLink
                    },
                    Status = "redirect"
                };

                PaymentDataResponse response = new PaymentDataResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = paymentData
                };

                watch.Stop();

                log($"::::: GetPaymentData Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: GetOrderPaymentData Result: {JsonConvert.SerializeObject(result)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new PaymentDataResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        public override async Task<OrderResponse> CreateOrderClone(OrderRequest request, ServerCallContext context)
        {
            try
            {
                int? currentUserId = context.TryToGetAppuserUserId();

                if (request.BuyId.StartsWith("vs", StringComparison.CurrentCultureIgnoreCase))
                {
                    request.TicketOwner = TicketOwner.VIP_SERVICE;
                }
                else if (!request.BuyId.StartsWith("vs") && !request.BuyId.StartsWith("eb") && !request.BuyId.StartsWith("da"))
                {
                    request.TicketOwner = TicketOwner.CHARTER_AGENT;
                }

                log($"::::: CREATE ORDER, UserId: {currentUserId} :::::");
                log($"::::: Request: {JsonConvert.SerializeObject(request)} :::::");

                var watch = Stopwatch.StartNew();

                var model = _mapper.Map<OrderRequest, OrderRequestDto>(request);

                model.AppUserId = currentUserId;

                var orderService = _orderFactoryCreator.GetOrderFactory(model.TicketOwner).OrderService();

                OrderResponseDto orderResponseDto = await orderService.CreateOrder(model);

                log($"::::: Order {orderService.GetType().Name} result: {JsonConvert.SerializeObject(orderResponseDto)} :::::");

                OrderResponse response = new OrderResponse() { Status = ResponseStatusType.Error };
                if (orderResponseDto.Status == StatusEnum.Ok)
                {
                    response.Status = ResponseStatusType.Ok;
                    response.Message = orderResponseDto.Message;
                    response.Data.Add(_mapper.Map<OrderDataDto, OrderDataClient>(orderResponseDto.Data));
                }

                watch.Stop();

                log($"::::: CREATE ORDER Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: Response: {JsonConvert.SerializeObject(response)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERROR CREATE ORDER: {ex.Message}");
                if (ex.InnerException != null)
                    _logger.LogError($"InnerException CREATE ORDER: {ex.InnerException.Message}");

                return new OrderResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<PaymentDataResponse> GetPaymentDataClone(PaymentDataRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: GetPaymentData, UserId: {currentUserId} :::::");

                await _orderDataService.ConnectOrderToUser(request.OrderId, currentUserId);

                var watch = Stopwatch.StartNew();

                var existingOrder = await _orderDataService.GetOrderById(request.OrderId);
                if (existingOrder.AppUserId != currentUserId)
                    throw new Exception("У вас нет такого заказа билетов.");

                var model = _mapper.Map<PaymentDataRequest, FlightPaymentDataInput>(request);
                model.BuyId = existingOrder.BuyId;
                model.AppUserId = currentUserId;

                var paymentService = _orderFactoryCreator.GetOrderFactory(existingOrder.TicketOwner).OrderPaymentService();

                PaymentDataBodyDto result = await paymentService.GetPaymentData(model);

                if (result.Status == StatusEnum.Error)
                    return new PaymentDataResponse() { Status = ResponseStatusType.Error, Message = result.Message };

                log($"::::: GetPaymentData {paymentService.GetType().Name} :::::");

                // Changed...
                //var paymentData = _mapper.Map<PaymentBody, PaymentBodyResponse>(result.Data);

                // Now Only SuccessRedirectLink is coming that we need ...
                var paymentData = new PaymentDataClient
                {
                    Payload = new PayloadDataClient
                    {
                        Result = result.Data.SucessRedirectLink
                    },
                    Status = "redirect"
                };

                PaymentDataResponse response = new PaymentDataResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = paymentData
                };

                watch.Stop();

                log($"::::: GetPaymentData Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: GetOrderPaymentData Result: {JsonConvert.SerializeObject(result)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new PaymentDataResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<PaymentDataResponse> GetPaymentInfo(PaymentInfoRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: GetPaymentData, UserId: {currentUserId} :::::");

                var watch = Stopwatch.StartNew();

                var existingOrder = await _orderDataService.GetOrderById(request.OrderId);
                if (existingOrder.AppUserId != currentUserId)
                    throw new Exception("У вас нет такого заказа билетов.");

                FlightPaymentDetailInput flightPaymentDetail = new FlightPaymentDetailInput
                {
                    OrderId = request.OrderId,
                    BuyId = existingOrder.BuyId,
                    AppUserId = currentUserId,
                    PaymentMethod = request.PaymentMethod,
                    AmountFromUserBalance = decimal.Parse(request.AmountFromUserBalance) / 100,
                    RequiredOnlinePayment = decimal.Parse(request.RequiredOnlinePayment) / 100
                };

                var paymentService = _orderFactoryCreator.GetOrderFactory(existingOrder.TicketOwner).OrderPaymentService();

                PaymentDataBodyDto result = await paymentService.GetPaymentInfo(flightPaymentDetail);

                if (result.Status == StatusEnum.Error)
                    return new PaymentDataResponse() { Status = ResponseStatusType.Error, Message = result.Message };

                log($"::::: GetPaymentData {paymentService.GetType().Name} :::::");

                // Now Only SuccessRedirectLink is coming that we need ...
                var paymentData = new PaymentDataClient
                {
                    Payload = new PayloadDataClient
                    {
                        Result = result.Data.SucessRedirectLink
                    },
                    Status = "redirect"
                };

                PaymentDataResponse response = new PaymentDataResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = paymentData
                };

                watch.Stop();

                log($"::::: GetPaymentData Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: GetOrderPaymentData Result: {JsonConvert.SerializeObject(result)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new PaymentDataResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<ConnectOrdersResponse> ConnectOrdersToUser(OrdersArrayRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: ConnectOrdersToUser, UserId: {currentUserId} :::::");

                var watch = Stopwatch.StartNew();

                foreach (int orderId in request.OrdersIds)
                {
                    await _orderDataService.ConnectOrderToUser(orderId, currentUserId);
                }

                watch.Stop();

                log($"::::: ConnectOrdersToUser Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");

                return new ConnectOrdersResponse
                {
                    Status = ResponseStatusType.Ok
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return new ConnectOrdersResponse
                {
                    Status = ResponseStatusType.Error
                };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<PaymentStatusResponse> CheckPaymentStatus(PaymentStatusRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: CheckPaymentStatus, UserId: {currentUserId} :::::");

                var watch = Stopwatch.StartNew();

                FlightPaymentStatusInput requestModel = new FlightPaymentStatusInput()
                {
                    AppUserId = currentUserId,
                    OrderId = request.OrderId
                };

                log($"::::: CheckPaymentStatus Request: {JsonConvert.SerializeObject(requestModel)} :::::");

                var existingOrder = await _orderDataService.GetOrderById(request.OrderId);

                if (existingOrder.AppUserId != currentUserId)
                {
                    throw new Exception("У вас нет такого заказа билетов.");
                }

                var paymentService = _orderFactoryCreator.GetOrderFactory(existingOrder.TicketOwner).OrderPaymentService();

                PaymentStatusDto result = await paymentService.SyncPaymentStatus(requestModel);

                log($"::::: CheckPaymentStatus {paymentService.GetType().Name} :::::");

                log($"::::: SyncPaymentStatus Result: {JsonConvert.SerializeObject(result)} :::::");

                var model = _mapper.Map<PaymentStatusDto, PaymentStatusDataClient>(result);
                PaymentStatusResponse response = new PaymentStatusResponse()
                {
                    Status = ResponseStatusType.Ok,
                    Data = model
                };

                watch.Stop();

                log($"::::: CheckPaymentStatus Response Time: {watch.ElapsedMilliseconds / 1000} sec OR {watch.ElapsedMilliseconds} ms :::::");
                log($"::::: CheckPaymentStatus Result: {JsonConvert.SerializeObject(response)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError($"ERROR CheckPaymentStatus: {ex.Message}");
                if (ex.InnerException != null)
                    _logger.LogError($"InnerException CheckPaymentStatus: {ex.InnerException.Message}");

                return new PaymentStatusResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<OrderResponse> GetOrders(EmptyRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: GetOrders, UserId: {currentUserId} :::::");

                var orders = _orderDataService.GetOrdersByUserId(currentUserId);

                CheckOrdersForNull(orders);

                var orderList = _mapper.Map<List<OrderDataDto>, List<OrderDataClient>>(orders);

                OrderResponse response = new OrderResponse();
                response.Status = ResponseStatusType.Ok;
                response.Data.Add(orderList);

                log($"::::: GetOrders Result: {JsonConvert.SerializeObject(response)} :::::");

                return Task.FromResult(response);
            }
            catch (Exception ex)
            {
                log($"::::: GetOrders Error: {ex.Message} :::::");
                if (ex.InnerException != null)
                    log($"::::: GetOrders InnerException Error: {ex.InnerException.Message} :::::");
                OrderResponse response = new OrderResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
                return Task.FromResult(response);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async override Task<PaginateOrdersResponse> GetPaginatedOrders(PaginationRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: GetOrders, UserId: {currentUserId} :::::");

                var orderResponse = await _orderDataService.GetPaginatedOrders(request.Page, request.Size, currentUserId);

                CheckOrdersForNull(orderResponse.Orders);

                var orderList = _mapper.Map<IList<OrderDataDto>, IList<OrderDataClient>>(orderResponse.Orders);

                PaginateOrdersResponse paginateOrdersResponse = new PaginateOrdersResponse
                {
                    Count = orderResponse.Count,
                    Status = ResponseStatusType.Ok
                };
                paginateOrdersResponse.Data.Add(orderList);

                return paginateOrdersResponse;
            }
            catch (Exception ex)
            {
                return new PaginateOrdersResponse { Status = ResponseStatusType.Error, Message = ex.Message };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<PassengerResponse> GetPassengers(EmptyRequest request, ServerCallContext context)

        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: GetPassengers, UserId: {currentUserId} :::::");

                var passengers = _passengerService.GetPassengers(currentUserId);

                CheckPassengersForNull(passengers, true);

                var passengerList = _mapper.Map<List<PassengersDto>, List<PassengersClient>>(passengers);

                PassengerResponse response = new PassengerResponse();
                response.Status = ResponseStatusType.Ok;
                response.Data = new PassengerDataClient();
                response.Data.Passengers.AddRange(passengerList);

                log($"::::: GetPassengers Result: {JsonConvert.SerializeObject(response)} :::::");

                return Task.FromResult(response);
            }
            catch (Exception ex)
            {
                log($"::::: GetPassengers Error: {ex.Message} :::::");
                if (ex.InnerException != null)
                    log($"::::: GetPassengers InnerException Error: {ex.InnerException.Message} :::::");
                PassengerResponse response = new PassengerResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
                return Task.FromResult(response);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<DeactivatedPassengerResponse> DeactivatePassenger(PassengersClient request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: DeactivatePassenger,  UserId: {currentUserId} :::::");

                var passengerMapped = _mapper.Map<PassengersClient, PassengersDto>(request);

                var deactivatedPassenger = await _passengerService.DeactivatePassenger(currentUserId, passengerMapped);

                log($"::::: DeactivatePassenger Result {JsonConvert.SerializeObject(deactivatedPassenger)} :::::");

                var mappedPassenger = _mapper.Map<PassengersDto, PassengersClient>(deactivatedPassenger);

                return new DeactivatedPassengerResponse
                {
                    Passenger = mappedPassenger,
                    Status = ResponseStatusType.Ok
                };

            }
            catch (Exception ex)
            {
                log($"::::: DeactivatePassenger Error: {ex.Message} :::::");
                if (ex.InnerException != null)
                    log($"::::: DeactivatePassenger InnerException Error: {ex.InnerException.Message} :::::");

                return new DeactivatedPassengerResponse
                {
                    Status = ResponseStatusType.Error,
                    Message = ex.Message
                };
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<StoredDataResponse> GetStoredData(EmptyRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();
                log($"::::: GetStoredData, UserId: {currentUserId} :::::");

                //get orders
                var orders = _orderDataService.GetOrdersByUserId(currentUserId);

                CheckOrdersForNull(orders);

                var orderList = _mapper.Map<List<OrderDataDto>, List<OrderDataClient>>(orders);

                //get passengers
                var passengers = _passengerService.GetPassengers(currentUserId);

                CheckPassengersForNull(passengers);

                var passengerList = _mapper.Map<List<PassengersDto>, List<PassengersClient>>(passengers);

                StoredDataResponse response = new StoredDataResponse() { Status = ResponseStatusType.Ok, Data = new StoredDataClient() };
                response.Data = new StoredDataClient();
                response.Data.Orders.Add(orderList);
                response.Data.Passengers.AddRange(passengerList);

                log($"::::: GetStoredData Result: {JsonConvert.SerializeObject(response)} :::::");

                return Task.FromResult(response);
            }
            catch (Exception ex)
            {
                log($"::::: GetStoredData Error: {ex.Message} :::::");
                if (ex.InnerException != null)
                    log($"::::: GetStoredData InnerException Error: {ex.InnerException.Message} :::::");
                StoredDataResponse response = new StoredDataResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
                return Task.FromResult(response);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<VoidOrderResponse> VoidOrder(VoidOrderRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: VoidOrder, UserId: {currentUserId} :::::");

                var existingOrder = await _orderDataService.GetOrderById(request.OrderId);
                if (existingOrder == null)
                    throw new Exception("Заказ не найден.");

                var orderCancelationService = _orderFactoryCreator.GetOrderFactory(existingOrder.TicketOwner).OrderCancelationService();

                var result = await orderCancelationService.VoidOrder(new FlightPaymentStatusInput() { AppUserId = currentUserId, OrderId = request.OrderId });

                VoidOrderResponse response = new VoidOrderResponse();
                response.Status = ResponseStatusType.Ok;
                response.Data = new VoidOrderDataClient()
                {
                    Status = result.Status.ToString(),
                    Message = result.Message,
                    OrderNewStatus = result.OrderNewStatus
                };

                log($"::::: VoidOrder {orderCancelationService.GetType().Name} result: {JsonConvert.SerializeObject(response)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                log($"::::: GetOrders Error: {ex.Message} :::::");
                if (ex.InnerException != null)
                    log($"::::: GetOrders InnerException Error: {ex.InnerException.Message} :::::");
                VoidOrderResponse response = new VoidOrderResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
                return response;
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<RefundOrderResponse> RefundOrder(RefundOrderRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: RefundOrder, UserId: {currentUserId} :::::");

                var orderCancelationService = _orderFactoryCreator.GetOrderFactory(TicketOwner.ETM).OrderCancelationService();

                var result = await orderCancelationService.RefundOrder(new FlightPaymentStatusInput() { AppUserId = currentUserId, OrderId = request.OrderId });

                RefundOrderResponse response = new RefundOrderResponse();
                response.Status = ResponseStatusType.Ok;
                response.Data = new RefundOrderDataClient()
                {
                    Status = result.Status.ToString(),
                    Message = result.Message,
                    OrderNewStatus = result.OrderNewStatus
                };

                log($"::::: RefundOrder Result: {JsonConvert.SerializeObject(response)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                log($"::::: GetOrders Error: {ex.Message} :::::");
                if (ex.InnerException != null)
                    log($"::::: GetOrders InnerException Error: {ex.InnerException.Message} :::::");
                RefundOrderResponse response = new RefundOrderResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
                return response;
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<UpdateOrderResponse> DeleteOrder(DeleteOrderRequest request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: DeleteOrder, OrderId:{request.OrderId}, UserId: {currentUserId} :::::");

                var result = await _orderDataService.DeleteOrder(request.OrderId, currentUserId);

                log($"::::: {result.Status.ToString()} :::::");

                UpdateOrderResponse response = new UpdateOrderResponse();
                response.Status = ResponseStatusType.Ok;
                response.Data = new UpdateOrderDataClient()
                {
                    Status = result.Status.ToString(),
                    Message = result.Message,
                };

                log($"::::: DeleteOrder Result: {JsonConvert.SerializeObject(response)} :::::");

                return response;
            }
            catch (Exception ex)
            {
                log($"::::: GetOrders Error: {ex.Message} :::::");
                if (ex.InnerException != null)
                    log($"::::: GetOrders InnerException Error: {ex.InnerException.Message} :::::");
                UpdateOrderResponse response = new UpdateOrderResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
                return response;
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override async Task<IsPassengerDeleted> DeletePassenger(PassengersClient request, ServerCallContext context)
        {
            try
            {
                int currentUserId = context.CurrentUserId();

                log($"::::: DeletePassenger,  UserId: {currentUserId} :::::");

                var passengerMapped = _mapper.Map<PassengersClient, PassengersDto>(request);

                bool IsPassengerDeleted = await _passengerService.DeletePassenger(currentUserId, passengerMapped);

                log($"::::: DeletePassenger Result {IsPassengerDeleted.ToString()} :::::");

                return new IsPassengerDeleted { IsDeleted = IsPassengerDeleted };

            }
            catch
            {
                return new IsPassengerDeleted
                {
                    IsDeleted = false
                };
            }
        }

        private void log(string message)
        {
            _logger.LogInformation($"\n\t::::: {message} :::::\n\t");
        }

        private void CheckOrdersForNull(IList<OrderDataDto> orders)
        {
            foreach (var order in orders)
            {
                foreach (var passenger in order.Passengers)
                {
                    if (passenger.FirstName is null)
                        passenger.FirstName = String.Empty;
                    if (passenger.LastName is null)
                        passenger.LastName = String.Empty;
                    if (passenger.MiddleName is null)
                        passenger.MiddleName = String.Empty;
                    if (passenger.Email is null)
                        passenger.Email = String.Empty;
                    if (passenger.PhoneNumber is null)
                        passenger.PhoneNumber = String.Empty;
                    if (passenger.PhoneNumberInfo is null)
                    {
                        passenger.PhoneNumberInfo = new CountryDto
                        {
                            CitizenshipCode = String.Empty,
                            Code = String.Empty,
                            CodeRus = String.Empty,
                            IsDefault = false,
                            Locale = String.Empty,
                            Name = String.Empty,
                            Phone = String.Empty
                        };
                    }

                    if (passenger.Type is null)
                    {
                        passenger.Type = new PassengerTypeDto
                        {
                            Code = String.Empty,
                            Description = String.Empty,
                            IsDefault = false,
                            Name = String.Empty
                        };
                    }

                    if (passenger.Gender is null)
                    {
                        passenger.Gender = new GenderDto
                        {
                            Code = String.Empty,
                            IsDefault = false,
                            Name = String.Empty
                        };
                    }
                    if (passenger.Document is null)
                    {
                        passenger.Document = new DocumentDto
                        {
                            Issue = String.Empty,
                            Expire = String.Empty,
                            Number = String.Empty,
                            Type = new DocumentTypeDto
                            {
                                Code = String.Empty,
                                Name = String.Empty,
                                Name1 = String.Empty,
                                IsDefault = false
                            }
                        };
                    }

                    if (passenger.Document.Type is null)
                    {
                        passenger.Document.Type = new DocumentTypeDto
                        {
                            Code = String.Empty,
                            Name = String.Empty,
                            Name1 = String.Empty,
                            IsDefault = false
                        };
                    }
                    if (passenger.Document.Issue is null)
                    {
                        passenger.Document.Issue = String.Empty;
                    }

                    if (passenger.Document.Expire is null)
                    {
                        passenger.Document.Expire = String.Empty;
                    }

                    if (passenger.Document.Number is null)
                    {
                        passenger.Document.Number = String.Empty;
                    }

                }
            }
        }

        private void CheckPassengersForNull(List<PassengersDto> passengers, bool shouldBeUpdated = false)
        {
            for (int i = 0; i < passengers.Count; i++)
            {
                if (passengers[i].FirstName is null)
                    passengers[i].FirstName = String.Empty;
                if (passengers[i].LastName is null)
                    passengers[i].LastName = String.Empty;
                if (passengers[i].MiddleName is null)
                    passengers[i].MiddleName = String.Empty;
                if (passengers[i].Email is null)
                    passengers[i].Email = String.Empty;
                if (passengers[i].PhoneNumber is null)
                    passengers[i].PhoneNumber = String.Empty;
                if (passengers[i].PhoneNumberInfo is null)
                {
                    passengers[i].PhoneNumberInfo = new CountryDto
                    {
                        CitizenshipCode = String.Empty,
                        Code = String.Empty,
                        CodeRus = String.Empty,
                        IsDefault = false,
                        Locale = String.Empty,
                        Name = String.Empty,
                        Phone = String.Empty
                    };
                }

                if (passengers[i].Type is null)
                {
                    passengers[i].Type = new PassengerTypeDto
                    {
                        Code = String.Empty,
                        Description = String.Empty,
                        IsDefault = false,
                        Name = String.Empty
                    };
                }

                if (passengers[i].Gender is null)
                {
                    passengers[i].Gender = new GenderDto
                    {
                        Code = String.Empty,
                        IsDefault = false,
                        Name = String.Empty
                    };
                }
                if (passengers[i].Document is null)
                {
                    passengers[i].Document = new DocumentDto
                    {
                        Issue = String.Empty,
                        Expire = String.Empty,
                        Number = String.Empty,
                        Type = new DocumentTypeDto
                        {
                            Code = String.Empty,
                            Name = String.Empty,
                            Name1 = String.Empty,
                            IsDefault = false
                        }
                    };
                }

                if (passengers[i].Document.Type is null)
                {
                    passengers[i].Document.Type = new DocumentTypeDto
                    {
                        Code = String.Empty,
                        Name = String.Empty,
                        Name1 = String.Empty,
                        IsDefault = false
                    };
                }
                if (passengers[i].Document.Issue is null)
                {
                    passengers[i].Document.Issue = String.Empty;
                }

                if (passengers[i].Document.Expire is null)
                {
                    passengers[i].Document.Expire = String.Empty;
                }

                if (passengers[i].Document.Number is null)
                {
                    passengers[i].Document.Number = String.Empty;
                }

                if (shouldBeUpdated)
                    _passengerService.SavePassenger(passengers[i]).GetAwaiter().GetResult();

            }
        }

    }
}
