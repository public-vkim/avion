﻿using AutoMapper;
using Avione.BusinessLogicLayer.Models.RailwayModels;
using Avione.Collection.gRpcProtos;
using Avione.Common.gRpcProtos;
using Avione.Infrastructure.DbContext.Entities;
using Avione.Infrastructure.DbContext.Repository;
using Avione.Search.gRpcServices.Base;
using Avione.ServiceLayer.Services;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avione.Search.gRpcServices.Services
{
    public class CollectionService: gRpcCollectionService.gRpcCollectionServiceBase
    {
        private readonly IMapper _mapper;
        private readonly IDictionaryService _dictionaryService;
        private readonly ILogger<CollectionService> _logger;
        private readonly IAppversionService _repository;

        public CollectionService(IMapper mapper,
                                 IDictionaryService dictionaryService,
                                 ILogger<CollectionService> logger,
                                 IAppversionService repository)
        {
            this._logger = logger;
            this._mapper = mapper;
            this._dictionaryService = dictionaryService;
            this._repository = repository;
        }

        public override Task<CollectionResponse> GetCollections(EmptyRequest request, ServerCallContext context)
        {
            try
            {
                log($"::::: CollectionService.GetCollections :::::");

                //get dictionaries
                var countries = _dictionaryService.GetCountries();
                var documentTypes = _dictionaryService.GetDocumentTypes();
                var flightClasses = _dictionaryService.GetFlightClasses();
                var genders = _dictionaryService.GetGenders();
                var passengerTypeQties = _dictionaryService.GetPassengerTypeQty();
                var passengerTypes = _dictionaryService.GetPassengerTypes();
                var paymentTypes = _dictionaryService.GetPaymentTypes();
                var regions = _dictionaryService.GetRegions();

                //mapping..
                var countriesMapped = _mapper.Map<List<CountryDto>, List<CountryClient>>(countries);
                var documentTypesMapped = _mapper.Map<List<DocumentTypeDto>, List<DocumentTypeClient>>(documentTypes);
                var flightClassesMapped = _mapper.Map<List<FlightClassDto>, List<FlightClassClient>>(flightClasses);
                var gendersMapped = _mapper.Map<List<GenderDto>, List<GenderClient>>(genders);
                var passengerTypeQtiesMapped = _mapper.Map<List<PassengerTypeQtyDto>, List<PassengerTypeQtyClient>>(passengerTypeQties);
                var passengerTypesMapped = _mapper.Map<List<PassengerTypeDto>, List<PassengerTypeClient>>(passengerTypes);
                var paymentTypesMapped = _mapper.Map<List<PaymentTypeDto>, List<PaymentTypeClient>>(paymentTypes);
                var regionsMapped = _mapper.Map<List<RegionDTO>, List<Region>>(regions);

                CollectionResponse response = new CollectionResponse() { Status = ResponseStatusType.Ok, Data = new  CollectionDataClient() };
                response.Data = new  CollectionDataClient();
                response.Data.Countries.AddRange(countriesMapped);
                response.Data.Genders.AddRange(gendersMapped);
                response.Data.PassengerTypes.AddRange(passengerTypesMapped);
                response.Data.PassengerTypeQty.AddRange(passengerTypeQtiesMapped);
                response.Data.DocumentTypes.AddRange(documentTypesMapped);
                response.Data.FlightClasses.AddRange(flightClassesMapped);
                response.Data.PaymentTypes.AddRange(paymentTypesMapped);
                response.Data.Regions.AddRange(regionsMapped);

                log($"::::: CollectionService.GetCollections Result: {JsonConvert.SerializeObject(response)} :::::");

                return Task.FromResult(response);
            }
            catch (Exception ex)
            {
                log($"::::: CollectionService.GetCollections Error: {ex.Message} :::::");
                if (ex.InnerException != null)
                    log($"::::: CollectionService.GetCollections InnerException Error: {ex.InnerException.Message} :::::");
                CollectionResponse response = new CollectionResponse() { Status = ResponseStatusType.Error, Message = ex.Message };
                return Task.FromResult(response);
            }
        }

        public override Task<MobileVersion> GetCurrentVersion(EmptyRequest request, ServerCallContext context)
        {
            var version = _repository.GetAll().OrderBy(p => p.Id).LastOrDefault();

            var entity = _mapper.Map<Appversion, MobileVersion>(version);

            return Task.FromResult(entity);
        }

        private void log(string message)
        {
            _logger.LogInformation($"\n\t::::: {message} :::::\n\t");
        }

    }
}
