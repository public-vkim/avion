﻿using AutoMapper;
using Avione.RailwaySearch.gRpcProtos;
using Avione.ServiceLayer.Constants;
using Avione.ServiceLayer.Models.Inputs.Railway;
using Avione.ServiceLayer.Services;
using Avione.ServiceLayer.Services.Order.Railway;
using Grpc.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Avione.Search.gRpcServices.Services
{
    public class RailwaySearchService : gRpcRailwaySearch.gRpcRailwaySearchBase
    {
        private readonly IUTYSearchService utySearchService;
        private readonly IMapper mapper;
        private readonly ILogger<RailwaySearchService> logger;
        private readonly IRailwayStationService railwayStationService;
        private readonly IHttpContextAccessor _accessor;
        public RailwaySearchService(
            IUTYSearchService utySearchService,
            IMapper mapper,
            ILogger<RailwaySearchService> logger,
            IHttpContextAccessor accessor,
            IRailwayStationService railwayStationService)
        {
            this.utySearchService = utySearchService;
            this.mapper = mapper;
            this.logger = logger;
            this.railwayStationService = railwayStationService;
            this._accessor = accessor;
            this._accessor.HttpContext.Request.Headers["DeviceType"] = CommonConstants.Mobile;
        }


        public override Task<StationsResponse> SearchStations(SearchStationRequest request, ServerCallContext context)
        {

            var stations = railwayStationService.GetByName(request.StationName).GetAwaiter().GetResult();

            var mappedStations = mapper.Map<Google.Protobuf.Collections.RepeatedField<StationsModel>>(stations);

            StationsResponse response = new StationsResponse();
            response.Stations.AddRange(mappedStations);

            return Task.FromResult(response);
        }
        public override Task<SearchStationReponseDto> SearchResult(SearchResultRequest request, ServerCallContext context)
        {
            try
            {

                var requestDto = mapper.Map<SearchStationsInput>(request);

                var response = utySearchService.SearchResult(requestDto).Result;


                SearchStationReponseDto searchStation = new SearchStationReponseDto();
                if (response.Error != null)
                {
                    searchStation.ErrorModel = new ErrorModelDto()
                    {
                        Error = response.Error.error,
                        Message = response.Error.message,
                        Path = response.Error.path,
                        Status = response.Error.status
                    };
                }
                else
                {
                    var stations = response.Stations;

                    searchStation.Stations = new StationsDto();

                    for (int i = 0; i < stations.Discount?.Count; i++)
                    {
                        searchStation.Stations.DiscountStations.Add(new DiscountInStations()
                        {
                            DateTo = stations.Discount[i].DateTo,
                            Type = stations.Discount[i].Type
                        });
                    }

                    for (int i = 0; i < stations.Express?.Direction?.Count; i++)
                    {
                        var trains = stations.Express.Direction[i].Trains;
                        var trainInStations = new Google.Protobuf.Collections.RepeatedField<TrainInStations>();

                        for (int j = 0; j < trains?.Count; j++)
                        {
                            for (int l = 0; l < trains[j].Train.Count; l++)
                            {
                                var trainInTrains = trains[j].Train[l];

                                RouteInStations routeInStations = new RouteInStations();
                                for (int p = 0; p < trainInTrains.Route.Station.Count; p++)
                                {
                                    routeInStations.Station.Add(trainInTrains.Route.Station[p]);
                                }
                                PlacesInStations placesInStations = new PlacesInStations();
                                var cars = trainInTrains.Places.Cars;

                                for (int o = 0; o < cars.Count; o++)
                                {
                                    var tariffInCars = cars[o]?.Tariffs?.Tariff;

                                    var gRpcTarifInCars = new Google.Protobuf.Collections.RepeatedField<TariffInStations>();
                                    for (int t = 0; t < tariffInCars?.Count; t++)
                                    {
                                        gRpcTarifInCars.Add(new TariffInStations
                                        {
                                            Owner = new OwnerInStations
                                            {
                                                Country = new CountryInStations
                                                {
                                                    Code = tariffInCars[t]?.Owner?.Country?.Code ?? string.Empty,
                                                    Name = tariffInCars[t]?.Owner?.Country?.Name ?? string.Empty,
                                                },
                                                Railway = new RailwayInStations
                                                {
                                                    Code = tariffInCars[t]?.Owner?.Railway?.Code ?? string.Empty,
                                                    Name = tariffInCars[t]?.Owner?.Railway?.Name ?? string.Empty,
                                                },
                                                Type = tariffInCars[t]?.Owner.Type ?? string.Empty,
                                            },
                                            ClassService = new ClassServiceInStations
                                            {
                                                Type = tariffInCars[t]?.ClassService?.Type ?? string.Empty,
                                                Content = tariffInCars[t]?.ClassService.Content ?? string.Empty,
                                                Service = new ServiceInStations
                                                {
                                                    Conditioner = tariffInCars[t]?.ClassService?.Service?.Conditioner ?? false,
                                                }
                                            },
                                            Carrier = new CarrierInStations
                                            {
                                                Name = tariffInCars[t]?.Carrier?.Name ?? string.Empty
                                            },
                                            ComissionFee = tariffInCars[t].ComissionFee ?? string.Empty,
                                            Tariff = tariffInCars[t].Tariff ?? string.Empty,
                                            TariffService = tariffInCars[t].TariffService ?? string.Empty,
                                            Seats = new SeatsInStations
                                            {
                                                FreeComp = tariffInCars[t].Seats.FreeComp ?? string.Empty,
                                                SeatsDn = tariffInCars[t].Seats.SeatsDn ?? string.Empty,
                                                SeatsLateralDn = tariffInCars[t].Seats.SeatsLateralDn ?? string.Empty,
                                                SeatsLateralUp = tariffInCars[t].Seats.SeatsLateralUp ?? string.Empty,
                                                SeatsUndef = tariffInCars[t].Seats.SeatsUndef ?? string.Empty,
                                                SeatsUp = tariffInCars[t].Seats.SeatsUp ?? string.Empty
                                            }
                                        });
                                    }
                                    placesInStations.Cars.Add(new CarsInStations
                                    {
                                        Type = cars[o]?.Type ?? string.Empty,
                                        IndexType = cars[o]?.IndexType ?? string.Empty,
                                        FreeSeats = cars[o]?.FreeSeats ?? string.Empty,
                                        TypeShow = cars[o]?.TypeShow ?? string.Empty,
                                        Seats = new SeatsInStations
                                        {
                                            FreeComp = cars[o]?.Seats.FreeComp ?? string.Empty,
                                            SeatsDn = cars[o]?.Seats.SeatsDn ?? string.Empty,
                                            SeatsLateralDn = cars[o]?.Seats.SeatsLateralDn ?? string.Empty,
                                            SeatsLateralUp = cars[o]?.Seats.SeatsLateralUp ?? string.Empty,
                                            SeatsUndef = cars[o]?.Seats.SeatsUndef ?? string.Empty,
                                            SeatsUp = cars[o]?.Seats.SeatsUp ?? string.Empty,
                                        },
                                        Tariffs = new TariffsInStations
                                        {
                                            FreeSeats = cars[o]?.Tariffs.FreeSeats ?? string.Empty,
                                            IndexType = cars[o]?.Tariffs?.IndexType ?? string.Empty,
                                            ShowType = cars[o]?.Tariffs?.ShowType ?? string.Empty
                                        }
                                    });
                                    placesInStations.Cars[o].Tariffs.Tariff.AddRange(gRpcTarifInCars);
                                }

                                trainInStations.Add(new TrainInStations()
                                {
                                    Length = trainInTrains?.Length ?? string.Empty,
                                    Type = trainInTrains?.Type ?? string.Empty,
                                    Number = trainInTrains?.Number ?? string.Empty,
                                    Number2 = trainInTrains?.Number2 ?? string.Empty,
                                    ElRegPossible = trainInTrains?.ElRegPossible ?? string.Empty,
                                    TimeInWay = trainInTrains?.TimeInWay ?? string.Empty,
                                    Brand = trainInTrains?.Brand ?? string.Empty,
                                    Arrival = new ArrivalInStations
                                    {
                                        Date = trainInTrains.Arrival?.Date ?? string.Empty,
                                        LocalDate = trainInTrains.Arrival?.LocalDate ?? string.Empty,
                                        LocalTime = trainInTrains.Arrival?.LocalTime ?? string.Empty,
                                        Stop = trainInTrains.Arrival?.Stop ?? string.Empty,
                                        Time = trainInTrains.Arrival?.Time ?? string.Empty,
                                    },
                                    Departure = new DepartureInStations
                                    {
                                        LocalDate = trainInTrains.Departure?.LocalDate ?? string.Empty,
                                        LocalTime = trainInTrains.Departure?.LocalTime ?? string.Empty,
                                        Stop = trainInTrains.Departure?.Stop ?? string.Empty,
                                        Time = trainInTrains.Departure?.Time ?? string.Empty
                                    },

                                    Route = routeInStations,
                                    Places = placesInStations

                                });
                            }

                        }

                        ExpressInStations expressInStations = new ExpressInStations();
                        expressInStations.Direction.Add(new DirectionInStations()
                        {
                            PassRoute = new PassRouteInStations
                            {
                                CodeFrom = stations.Express.Direction[i].PassRoute?.CodeFrom ?? string.Empty,
                                CodeTo = stations.Express.Direction[i].PassRoute?.CodeTo ?? string.Empty,
                                From = stations.Express.Direction[i].PassRoute?.From ?? string.Empty,
                                To = stations.Express.Direction[i].PassRoute?.To ?? string.Empty,
                            },
                            Type = stations.Express.Direction[i].Type,
                        });

                        searchStation.Stations.Express = expressInStations;

                        searchStation.Stations.Express.Direction[i].Trains.Add(new TrainsInStations { Date = stations.Express.Direction[i].Trains[i].Date });
                        searchStation.Stations.Express.Direction[i].Trains[i].Train.AddRange(trainInStations);
                    }

                }

                return Task.FromResult(searchStation);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex.Message, ex.InnerException);
                string innerException = ex.InnerException is null ? string.Empty : ex.InnerException.ToString();
                return Task.FromResult(new SearchStationReponseDto
                {
                    ErrorModel = new ErrorModelDto
                    {
                        Error = innerException,
                        Message = ex.Message,
                        Status = 500
                    }
                });
            }
        }
        public override Task<PlaceResponseDto> TrainsType(PlaceRequest request, ServerCallContext context)
        {
            try
            {
                var searchRequest = mapper.Map<PlaceInput>(request);
                var response = utySearchService.TrainsType(searchRequest).Result;

                PlaceResponseDto placeResponseDto = new PlaceResponseDto();

                if (response.Error != null)
                {
                    placeResponseDto.Error = new ErrorModelDto
                    {
                        Error = response.Error.error,
                        Message = response.Error.message,
                        Path = response.Error.path,
                        Status = response.Error.status,

                    };
                }
                else
                {
                    Place place = new Place();
                    place.HasError = response.Place.HasError;
                    place.Type = response.Place.Type;
                    placeResponseDto.Place = place;

                    for (int i = 0; i < response.Place.Direction.Count; i++)
                    {
                        DirectionInPlace directionInPlace = new DirectionInPlace();

                        var direction = response.Place.Direction[i];
                        directionInPlace.PassRoute = new PassRouteInPlace
                        {
                            CodeFrom = direction.PassRoute?.CodeFrom ?? string.Empty,
                            CodeTo = direction.PassRoute?.CodeTo ?? string.Empty,
                            From = direction.PassRoute?.From ?? string.Empty,
                            To = direction.PassRoute?.To ?? string.Empty,
                        };
                        directionInPlace.Type = direction.Type;
                        directionInPlace.HasError = direction.HasError;

                        IList<TrainsInPlace> gRpcTrains = new List<TrainsInPlace>();

                        var trains = direction.Trains;

                        for (int j = 0; j < trains?.Count; j++)
                        {
                            RouteInPlace routeInPlace = new RouteInPlace();

                            for (int p = 0; p < trains[j].Train.Route.Station.Count; p++)
                            {
                                routeInPlace.Station.Add(trains[j].Train.Route.Station[p]);
                            }
                            gRpcTrains.Add(new TrainsInPlace
                            {
                                Date = trains[j]?.Date,
                                Train = new TrainInPlace
                                {
                                    TrainBrandName = trains[j]?.Train?.TrainBrandName ?? string.Empty,
                                    TrainSchemaType = trains[j]?.Train?.TrainSchemaType ?? string.Empty,
                                    Length = trains[j]?.Train?.Length ?? string.Empty,
                                    Type = trains[j]?.Train?.Type ?? string.Empty,
                                    Number = trains[j]?.Train?.Number ?? string.Empty,
                                    Number2 = trains[j]?.Train?.Number2 ?? string.Empty,
                                    TimeInWay = trains[j]?.Train?.TimeInWay ?? string.Empty,
                                    Route = routeInPlace,
                                    Arrival = new ArrivalInPlace()
                                    {
                                        Date = trains[j]?.Train?.Arrival?.Date ?? string.Empty,
                                        LocalDate = trains[j]?.Train?.Arrival?.LocalDate ?? string.Empty,
                                        LocalTime = trains[j]?.Train?.Arrival?.LocalTime ?? string.Empty,
                                        Stop = trains[j]?.Train.Arrival?.Stop ?? string.Empty,
                                        Time = trains[j]?.Train.Arrival?.Time ?? string.Empty,
                                    },
                                    Departure = new DepartureInPlace()
                                    {
                                        LocalDate = trains[j]?.Train?.Departure?.LocalDate ?? string.Empty,
                                        LocalTime = trains[j]?.Train?.Departure?.LocalTime ?? string.Empty,
                                        Time = trains[j]?.Train?.Departure?.Time ?? string.Empty,
                                        Stop = trains[j]?.Train?.Departure?.Stop ?? string.Empty,

                                    },
                                    Brand = trains[j]?.Train?.Brand
                                }
                            });

                            IList<CarsInPlace> carsInPlace = new List<CarsInPlace>();

                            for (int l = 0; l < trains[j].Train.Cars.Count; l++)
                            {
                                var cars = trains[j].Train.Cars[l];

                                carsInPlace.Add(new CarsInPlace
                                {
                                    Type = cars?.Type ?? string.Empty,
                                    TypeShow = cars?.TypeShow ?? string.Empty,
                                    TrainLetter = cars?.TrainLetter ?? string.Empty,
                                    ComissionFee = cars?.ComissionFee ?? string.Empty,
                                    Tariff = cars?.Tariff ?? string.Empty,
                                    Owner = new OwnerInPlace
                                    {
                                        Type = cars.Owner.Type ?? string.Empty,
                                        Country = new CountryInPlace
                                        {
                                            Code = cars?.Owner?.Country?.Code ?? string.Empty,
                                            Name = cars?.Owner?.Country?.Name ?? string.Empty,
                                        },
                                        Railway = new RailwayInPlace
                                        {
                                            Code = cars?.Owner?.Railway?.Code ?? string.Empty,
                                            Name = cars?.Owner?.Railway?.Name ?? string.Empty,
                                        }
                                    },
                                    ClassService = new ClassServiceInPlace
                                    {
                                        Type = cars?.ClassService?.Type ?? string.Empty,
                                        Content = cars?.ClassService?.Content ?? string.Empty,
                                        Service = new ServiceInStations
                                        {
                                            Conditioner = cars.ClassService.Service.Conditioner
                                        }
                                    }
                                });

                                IList<CarInPlace> carInPlace = new List<CarInPlace>();

                                for (int o = 0; o < cars.Car.Count; o++)
                                {
                                    carInPlace.Add(new CarInPlace
                                    {
                                        Number = cars.Car[o]?.Number ?? string.Empty,
                                        SubType = cars.Car[o]?.SubType ?? string.Empty,
                                        Places = cars.Car[o]?.Places ?? string.Empty,
                                        Seats = new SeatsInPlace
                                        {
                                            FreeComp = cars?.Car[o]?.Seats?.FreeComp ?? string.Empty,
                                            SeatsDn = cars?.Car[o]?.Seats?.SeatsDn ?? string.Empty,
                                            SeatsLateralDn = cars?.Car[o]?.Seats?.SeatsLateralDn ?? string.Empty,
                                            SeatsLateralUp = cars?.Car[o]?.Seats?.SeatsLateralUp ?? string.Empty,
                                            SeatsUndef = cars?.Car[o]?.Seats?.SeatsUndef ?? string.Empty,
                                            SeatsUp = cars?.Car[o].Seats?.SeatsUp ?? string.Empty,
                                        },
                                        TypeOfCarSchema = new TypeOfCarSchema
                                        {
                                            BarndName = cars.Car[o]?.TypeOfCarSchema?.BarndName ?? string.Empty,
                                            CarNumber = cars.Car[o]?.TypeOfCarSchema?.CarNumber ?? string.Empty,
                                            ClassService = cars.Car[o]?.TypeOfCarSchema?.ClassService ?? string.Empty,
                                            SchemaName = cars?.Car[o]?.TypeOfCarSchema?.SchemaName ?? string.Empty,
                                            SeatsCount = cars.Car[o]?.TypeOfCarSchema?.SeatsCount ?? string.Empty
                                        }
                                    });
                                }
                                carsInPlace[l].Car.AddRange(carInPlace);
                            }
                            gRpcTrains[j].Train.Cars.AddRange(carsInPlace);
                        }
                        directionInPlace.Trains.AddRange(gRpcTrains);
                        placeResponseDto.Place.Directions.Add(directionInPlace);
                    }
                }

                return Task.FromResult(placeResponseDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex.Message, ex.InnerException);
                string innerException = ex.InnerException is null ? string.Empty : ex.InnerException.ToString();
                return Task.FromResult(new PlaceResponseDto
                {
                    Error = new ErrorModelDto
                    {
                        Error = innerException,
                        Message = ex.Message,
                        Status = 500
                    }
                });
            }

        }
    }
}
